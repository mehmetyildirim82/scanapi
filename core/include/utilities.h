//
// Created by yildirim on 07.06.15.
//

#ifndef ORIENTSCAN3D_UTILITIES_H
#define ORIENTSCAN3D_UTILITIES_H

#include <geometric.h>

namespace cook {

    bool CheckFile(const char* filename);

    void Generate2dGrid(Rect rect, Point2dArray& data);

} // namespace cook

#endif //ORIENTSCAN3D_UTILITIES_H
