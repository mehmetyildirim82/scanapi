//
// Created by yildirim on 03.06.15.
//

#ifndef ORIENTSCAN3D_EXCEPTIONS_H
#define ORIENTSCAN3D_EXCEPTIONS_H

#include <exception>
#include <string>

namespace cook{

    class exceptionOpenCV : public std::exception{
    public:
        virtual const char* what(){
            return "OpenCV related error happened";
        };
    };

    class exceptionMatrixSize : public std::exception{
    public:
        virtual const char* what(){
            return "Size mismatch";
        };
    };

    class exceptionNumerical : public std::exception{
    public:
        virtual ~exceptionNumerical() throw (){}

        explicit exceptionNumerical(const char* msg) {
            this->msg = msg;
        }

        virtual const char* what() const throw() {
            return msg.c_str();
        };

    private:
        std::string msg;
    };

} // namespace cook

#endif //ORIENTSCAN3D_EXCEPTIONS_H
