//
// Created by yildirim on 05.07.15.
//

#ifndef ORIENTSCAN3D_GEOMETRIC_H
#define ORIENTSCAN3D_GEOMETRIC_H

#include <basic_types.h>

namespace cook {

        struct Rect{
            Rect(int tlx, int tly, int brx, int bry) {
                topLeft.x = tlx;
                topLeft.y = tly;
                bottomRight.x = brx;
                bottomRight.y = bry;
            }
            Point2f topLeft;
            Point2f bottomRight;
        };

        struct Line2d {
            Point2f center;
            Point2f direction;
        };

        void ConvertLinearEqTo2DLine(const Point3f& lineEq, const int x, const int length, Line2d& line2d);

        double AngleBetween(const Point2f& from, const Point2f& to);
} // namespace cook

#endif //ORIENTSCAN3D_GEOMETRIC_H
