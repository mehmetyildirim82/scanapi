//
// Created by yildirim on 27.06.15.
//

#ifndef ORIENTSCAN3D_NUMERICAL_H
#define ORIENTSCAN3D_NUMERICAL_H

/*! @file numerical.h
 * Contains numerical methods for solving linea equations
*/

namespace cook {

    /*! Householders method for QR Decomposition.
    \param M the input matrix.
    \param Z row count.
    \param S column count.
    \return true if successful.
    */
    bool Householder(double **M, int Z, int S);

    /*! Solves a 3x3 linear equation
    \param A design matrix.
    \param y array of knowns.
    \param x array of unknowns.
    \param Eps values lower than this indicates zero solution.
    \return true if successfull.
    */
    bool Solve3x3LinearEq(double A[][3], double *y, double x[], double Eps);

} // namespace cook

#endif //ORIENTSCAN3D_NUMERICAL_H
