//
// Created by yildirim on 02.06.15.
//

#ifndef ORIENTSCAN3D_COOKMAT_H
#define ORIENTSCAN3D_COOKMAT_H

#include "basic_types.h"

namespace cook{

        /*!
         * @brief Mat Class. Wrapper around cv::Mat
         * @class Mat mat.h
         * @author Mehmet Yildirim
         * \ingroup Transformations
         * @{
         */
        class Mat{
        public:
            /*!
             * \brief Mat default constructor
             */
            Mat();

            ~Mat();

            Mat(const Mat& copy);

            /*!
             * \brief Mat constructor with size given
             */
            Mat(int rows, int cols); /* throw exceptionOpenCv */

            /*!
             * \brief Mat constructor with an array of value
             */
            Mat(int rows, int cols, double data[]); /* throw exceptionOpenCv */

            /*!
             * \brief Mat constructor
             * \param point3f
             */
            Mat(const Point3f in);

            /*!
             * \brief Mat constructor
             * \param point2f
             */
            Mat(const Point2f in);

            /*!
             * \brief Mat constructor
             * \param cv::Mat
             */
            Mat(const cv::Mat& in);

            /*!
             * \brief Mat constructor
             * \param cv::MatExpr
             */
            Mat(const cv::MatExpr in);

            Mat& operator = (const cv::Mat& m);
            Mat& operator = (const Mat& m);
            Mat& operator = (const cv::MatExpr& expr);

            /*!
            * \brief At is a wrapper around cv::Mat::at<> function template
            */
            double& At(int x, int y);

            cv::Mat& Get();
            cv::Mat GetReadOnly() const;

            // TODO: Inefficiency!!! use memcpy
            void DeleteLasRow() {
                size_t size = mat.rows;
                cv::Mat res(size-1, 1, CV_64F);
                for(size_t i = 0; i < size-1; i++ ) {
                    res.at<double>(i) = mat.at<double>(i);
                }
                mat = res;
            }

        protected:
            cv::Mat mat;
        };

        /*!
        * \brief Mat comparison
        */
        bool operator==(const Mat& lhs, const Mat& rhs);

        /*! @}*/

} // namespace cook

#endif //ORIENTSCAN3D_COOKMAT_H
