//
// Created by yildirim on 02.06.15.
//

#ifndef ORIENTSCAN3D_BASIC_TYPES_H
#define ORIENTSCAN3D_BASIC_TYPES_H

/*! @file basic_types.h
 * Contains the basic core structures shared over the other modules
*/

#include <core_definitions.h>
#include <opencv2/core/core.hpp>

#define _USE_MATH_DEFINES

namespace cook{

#if 0
#ifdef __GNUC__
    typedef char int8;
    typedef short int16;
    typedef int int32;
    typedef long int64;
    typedef unsigned char uint8;
    typedef unsigned short uint16;
    typedef unsigned int uint32;
    typedef unsigned long uint64;
#else
    typedef char int8;
    typedef short int16;
    typedef int int32;
    typedef long long int64;
    typedef unsigned char uint8;
    typedef unsigned short uint16;
    typedef unsigned int uint32;
    typedef unsigned long long uint64;
#endif
#endif

    typedef cv::Point2f Point2f;
    typedef cv::Point3f Point3f;
    typedef std::vector<Point2f> Point2dArray;
    typedef std::vector<Point3f> Point3dArray;
    typedef cv::Mat Image;
    typedef cv::Size ImageSize;
    typedef std::vector<Image> ImageSequence;

} // namespace cook

#endif //ORIENTSCAN3D_BASIC_TYPES_H
