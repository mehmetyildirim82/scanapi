//
// Created by yildirim on 28.06.15.
//

#include "utilities.h"
#include <fstream>

namespace cook {

        bool CheckFile(const char* filename) {
            return std::ifstream(filename);
        }

        void Generate2dGrid(Rect rect, Point2dArray& data) {
            for(int r = rect.topLeft.y; r < rect.bottomRight.y; r++)
            {
                for(int c = rect.topLeft.x; c < rect.bottomRight.x; c++)
                {
                    data.push_back(Point2f(c, r));
                }
            }
        }

} // namespace cook