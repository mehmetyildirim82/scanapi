//
// Created by yildirim on 05.07.15.
//
#include "geometric.h"
#include <math.h>

namespace cook {

        void ConvertLinearEqTo2DLine(const Point3f& lineEq, const int x, const int length, Line2d& line2d) {
            line2d.center.x = x;
            line2d.center.y = -(lineEq.z/lineEq.y);
            double m = -(lineEq.x / lineEq.y);
            double angle = atan(m);
            line2d.direction.x = length*cos(angle);
            line2d.direction.y = length*sin(angle);
        }

        double AngleBetween(const Point2f& from, const Point2f& to)
        {
            double pt1x = from.x;
            double pt1y = from.y;
            double pt2x = to.x;
            double pt2y = to.y;

            double len1 = sqrt(pt1x * pt1x + pt1y * pt1y);
            double len2 = sqrt(pt2x * pt2x + pt2y * pt2y);

            double dot = pt1x * pt2x + pt1y * pt2y;

            double a = dot / (len1 * len2);

            if (a >= 1.0)
                return 0.0;
            else if (a <= -1.0)
                return M_PI;
            else
                return std::acos(a); // 0..PI
        }
} // namespace cook