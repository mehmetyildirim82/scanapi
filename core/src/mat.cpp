//
// Created by yildirim on 02.06.15.
//

#include <exceptions.h>
#include "mat.h"

namespace cook {

        Mat::Mat() {
            //
        }

        Mat::~Mat() {
            //
        }

        Mat::Mat(const Mat& copy) {
            mat = copy.mat.clone();
        }

        Mat::Mat(int rows, int cols) {
            try {
                mat = cv::Mat(rows, cols, CV_64F);
            }
            catch (...) {
                throw exceptionOpenCV();
            }
        }

        Mat::Mat(int rows, int cols, double data[]) {
            try {
                mat = cv::Mat(rows, cols, CV_64F, data);
            }
            catch (...) {
                throw exceptionOpenCV();
            }
        }

        Mat::Mat(const Point3f in) {
            cv::Mat temp(in);
            temp.convertTo(mat, CV_64F);
        }

        Mat::Mat(const Point2f in) {
            cv::Mat temp(in);
            temp.convertTo(mat, CV_64F);
        }

        Mat::Mat(const cv::Mat& in) {
            mat = in;
        }

        Mat::Mat(const cv::MatExpr in) {
            mat = in;
        }

        Mat& Mat::operator = (const cv::Mat& m) {
            mat = m;
            return (*this);
        }

        Mat& Mat::operator = (const Mat& m) {
            mat = m.mat.clone();
            return (*this);
        }

        Mat& Mat::operator = (const cv::MatExpr& expr) {
            mat = expr;
            return (*this);
        }

        double& Mat::At(int row, int col) {
            try{
                return mat.at<double>(cv::Point2i(col, row));
            }
            catch(...){
                throw exceptionOpenCV();
            }
        }

        cv::Mat& Mat::Get() {
            return mat;
        }

        cv::Mat Mat::GetReadOnly() const {
            return mat;
        }

        bool operator==(const Mat& lhs, const Mat& rhs) {
            return true;
        }

} // namespace cook