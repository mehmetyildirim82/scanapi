#include <gtest/gtest.h>
#include <opencv2/core/core.hpp>

/// Test here dependencies

TEST(GTest, DependencyCheck) {
    EXPECT_EQ(0, 0);
}

TEST(OpenCV, DependencyCheck) {
    cv::Mat m = cv::Mat::zeros(3, 3, CV_8UC1);
    EXPECT_EQ(0, m.at < unsigned
            char > (0));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}