#include <gtest/gtest.h>
#include <basic_types.h>
#include <exceptions.h>

TEST(PrimitivesTest, Sizes) {
    EXPECT_EQ(1, sizeof(cook::uint8));
    EXPECT_EQ(1, sizeof(cook::int8));
    EXPECT_EQ(2, sizeof(cook::uint16));
    EXPECT_EQ(2, sizeof(cook::int16));
    EXPECT_EQ(4, sizeof(cook::uint32));
    EXPECT_EQ(4, sizeof(cook::int32));
    EXPECT_EQ(8, sizeof(cook::uint64));
    EXPECT_EQ(8, sizeof(cook::int64));
}

TEST(ExceptionsTest, CustomExceptions){
    EXPECT_THROW( throw cook::exceptionOpenCV(), cook::exceptionOpenCV );

    try{
        throw cook::exceptionOpenCV();
    }
    catch(cook::exceptionOpenCV& e) {
        const char *error = e.what();
        EXPECT_EQ("OpenCV related error happened", error);
    }
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}