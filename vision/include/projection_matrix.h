//
// Created by yildirim on 01.06.15.
//

#ifndef ORIENTSCAN3D_PROJECTIONMATRIX_H
#define ORIENTSCAN3D_PROJECTIONMATRIX_H

#include <mat.h>

namespace cook{
    namespace vision{

        class IntrinsicMat : public Mat {
        public:
            IntrinsicMat() : Mat( 3, 3 ) {};
        };

        /*!
         * @brief ProjectionMatrix Class. Projects world points into image plane, according to pinhole camera model.
         * @class ProjectionMatrix projection_matrix.h
         * @author Mehmet Yildirim
         * \ingroup Transformations
         * @{
         */
        class ProjectionMatrix : public Mat {
        public:
            //TODO: Documentation
            ProjectionMatrix();
            ProjectionMatrix(double data[]); /* throw exceptionMatrixSize */
            ProjectionMatrix(Mat& mat); /* throw exceptionMatrixSize */
            ProjectionMatrix(const ProjectionMatrix& projMat);

            ProjectionMatrix& operator=(const ProjectionMatrix& rhs);

            ~ProjectionMatrix();

            bool Decompose(IntrinsicMat& K, Mat& rotation, Mat& translation)const;
            bool Compose(const IntrinsicMat& K, const Mat& rotation, const Mat& translation);

            Point2f Project(const Point3f& p3d)const;

            float TestProjection(const Point3f& p3f, const Point2f& p2f)const;
        };
        /*! @}*/

    } // namespace vision
} //namespace cook

#endif //ORIENTSCAN3D_PROJECTIONMATRIX_H
