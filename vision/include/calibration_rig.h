//
// Created by yildirim on 19.06.15.
//

#ifndef ORIENTSCAN3D_CALIBRATION_RIG_H
#define ORIENTSCAN3D_CALIBRATION_RIG_H

#include <marker_data.h>

namespace cook {
    namespace vision{

        /*!
         * @brief CalibrationRig Struct. Contains information for associated physical calibration rig.
         * A calibration rig consist of ring markers and ID markers.
         * @struct CalibrationRig calibration_rig.h
         * @author Mehmet Yildirim
         * \ingroup Calibration
         * @{
         */
        struct CalibrationRig {
            /// Total amount of markers
            unsigned int    countMarker;

            /// Total amount of planes on which the markers are distributed
            unsigned char   countPlane;

            /// Array of id markers
            IdMarkerArray3d idMarkers;

            /// Array of ring markers
            MarkerArray3d   ringMarkers;
        };
        /*! @}*/

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CALIBRATION_RIG_H
