//
// Created by yildirim on 27.06.15.
//

#ifndef ORIENTSCAN3D_SCAN_DATA_H
#define ORIENTSCAN3D_SCAN_DATA_H

#include <memory>
#include <types_vision.h>

namespace cook {
    namespace vision{

        //TODO: Documentation!
        class ScanData {
        public:
            ScanData() :
                    activeSection(0)
            {};

            virtual bool AddImage(Image scan_image)=0;

            void ClearBuffer(){
                activeSection = 0;
                for(std::size_t i = 0; i < sequences.size(); i++) {
                    ImageSequence empty;
                    sequences[i].swap(empty);
                }
            };

        protected:
            virtual void PreProcess(Image& scan_image) = 0;

            std::vector<ImageSequence> sequences;
            std::vector<int> sectionSize;
            int activeSection;

		public:
			typedef std::shared_ptr<ScanData> Ptr;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_SCAN_DATA_H
