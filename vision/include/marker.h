//
// Created by yildirim on 08.06.15.
//

#ifndef ORIENTSCAN3D_MARKER_H
#define ORIENTSCAN3D_MARKER_H

#include <marker_data.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        class Marker {
        public:
            Marker():
                    //Default Parameters
                    minSize(0.0,0.0),
                    maxSize(1000.0,1000.0)
            {};

            virtual ~Marker(){};
            virtual bool FindSelf(const ContourArray& contours, MarkerArray2d& markers, Image* mask=NULL, Image* outImg=NULL) const;

            typedef std::shared_ptr<Marker> Ptr;

            cv::Size2f& GetMinSize() {return minSize;};
            cv::Size2f& GetMaxSize() {return maxSize;};

        protected:
            cv::Size2f minSize;
            cv::Size2f maxSize;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_MARKER_H

