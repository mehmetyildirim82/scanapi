//
// Created by yildirim on 19.06.15.
//

#ifndef ORIENTSCAN3D_VISUALIZATION_H
#define ORIENTSCAN3D_VISUALIZATION_H

#include <marker_data.h>
#include <geometric.h>

namespace cook {
    namespace vision {

        Image Visualize(const Image& imgIn, IdMarkerArray2d& id_markers, const char* filename = NULL);
        Image Visualize(const Image& imgIn, Point2dArray& markers, const char* filename = NULL);
        Image Visualize(const Image& imgIn, Line2d l, const char* filename = NULL, int thickness = 1);

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_VISUALIZATION_H
