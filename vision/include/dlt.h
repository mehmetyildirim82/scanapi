//
// Created by yildirim on 05.06.15.
//

#ifndef ORIENTSCAN3D_DLT_H
#define ORIENTSCAN3D_DLT_H

//#include <types_vision.h>
#include <projection_matrix.h>

namespace cook {
    namespace vision {

        /*!
        * @brief Direct Linear Transformation, calculates a matrix that maps 3d points on 2d space
        * @author Mehmet Yildirim
        * \ingroup Transformations
        * @{
        */
        bool DLT(const Point3dArray& p3d, const Point2dArray& p2d, ProjectionMatrix& P);
        /*! @}*/

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_DLT_H
