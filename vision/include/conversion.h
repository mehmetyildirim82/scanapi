//
// Created by yildirim on 05.06.15.
//

#ifndef ORIENTSCAN3D_CONVERSION_H
#define ORIENTSCAN3D_CONVERSION_H

#include <mat.h>
#include <geometric.h>

namespace cook{
    namespace vision{

        /// Converts Point to homogeneous point in matrix format
        template<typename T>
        Mat Homogenize(const T& in) {
            cv::Mat result(in);
            result.convertTo(result, CV_64F);
            cv::Mat row = cv::Mat::eye(1,1,CV_64F);
            result.push_back(row);
            return Mat(result);
        }

        /// Normalizes a Homogenous matrix by dividing each elements by w
        void NormaliseHomogenous(Mat& mat_homo);

        //TODO: documentation
        template <typename T>
        Mat HomogenizeAndMultiply(const T &in, const Mat &factor) {
                Mat homo = Homogenize<T>(in);
                Mat result = factor.GetReadOnly()*homo.Get();
                return result;
        }

        //Mat HomogenizeAndMultiply(const Point2f &in, const Mat &factor);

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CONVERSION_H
