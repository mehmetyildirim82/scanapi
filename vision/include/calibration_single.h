//
// Created by yildirim on 20.06.15.
//

#ifndef ORIENTSCAN3D_CALIBRATION_SINGLE_H
#define ORIENTSCAN3D_CALIBRATION_SINGLE_H

#include <correspondence.h>
#include <camera_model.h>
#include <projector_model.h>

namespace cook {
    namespace vision {

        class CalibrationSingle {
        public:
            double Calibrate(const Correspondence3d& correspondence, CameraModel& model);
            double Calibrate(const CorrespondenceAbsolute& correspondence, CameraModel& camera, ProjectorModel& projector);
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CALIBRATION_SINGLE_H
