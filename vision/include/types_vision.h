//
// Created by yildirim on 02.06.15.
//

#ifndef ORIENTSCAN3D_TYPES_H
#define ORIENTSCAN3D_TYPES_H

#include <memory>
#include <vector>
#include <basic_types.h>

namespace cook{
    namespace vision{

        typedef std::vector<cv::Point> Contour;
        typedef std::vector<Contour> ContourArray;
        typedef cv::Mat DistortionCoeff;
        typedef cv::Mat RotationMatrix;
        typedef cv::Mat TranslationMatrix;

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_TYPES_H
