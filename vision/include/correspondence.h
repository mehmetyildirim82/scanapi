//
// Created by yildirim on 21.06.15.
//

#ifndef ORIENTSCAN3D_CORRESPONDENCE_H
#define ORIENTSCAN3D_CORRESPONDENCE_H

#include <marker_data.h>
#include <interpolation.h>

namespace cook {
    namespace vision {

        //TODO: Documentation
        template <typename T_From, typename T_To>
        struct Correspondence {
            std::vector<unsigned int> index;
            T_From from;
            T_To to;
        };
        typedef Correspondence<Point3dArray, Point2dArray> Correspondence3d;
        typedef Correspondence<Point2dArray, Point2dArray> Correspondence2d;

        struct CorrespondenceAbsolute {
            Correspondence3d geometric;
            std::vector<double> absoluteValue;

            CorrespondenceAbsolute() {}

            CorrespondenceAbsolute(const Image& absPhase, const Correspondence3d& corr) {
                for(std::size_t i = 0; i < corr.to.size(); i++) {
                    double W = BilinearInterpolation<short>(corr.to[i], absPhase);
                    //if(W < 10e-5)
                    //    continue;
                    absoluteValue.push_back(W);
                    geometric.from.push_back(corr.from[i]);
                    geometric.to.push_back(corr.to[i]);

                    //No indexes are copied
                    //No need to copy yet!!!
                }
            }
        };

        struct StereoCorrespondence {
            std::vector<unsigned int> index;
            Point3dArray world;
            Point2dArray image01;
            Point2dArray image02;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CORRESPONDENCE_H
