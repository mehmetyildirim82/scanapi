//
// Created by yildirim on 21.06.15.
//

#ifndef ORIENTSCAN3D_CALIBRATION_STEREO_H
#define ORIENTSCAN3D_CALIBRATION_STEREO_H

#include <stereo_model.h>
#include <correspondence.h>

namespace cook {
    namespace vision{

        //TODO: Documentation!
        class CalibrationStereo {
        public:
            double Calibrate(const StereoCorrespondence& correspondence,
                             StereoModel& model);
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CALIBRATION_STEREO_H
