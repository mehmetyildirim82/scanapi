//
// Created by yildirim on 08.06.15.
//

#ifndef ORIENTSCAN3D_CONTOUREXTRACTOR_H
#define ORIENTSCAN3D_CONTOUREXTRACTOR_H

#include <types_vision.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        class ContourExtractor {
        public:
            ContourExtractor():
                    //Default parameters
                    binaryThreshold(100)
            {};

            bool Extract(const Image& input, ContourArray& contours);
            const Image& GetContourImage() const;
            const Image& GetBinaryImage() const;

            unsigned char& GetBinaryThreshold() {return binaryThreshold;};

            typedef std::shared_ptr<ContourExtractor> Ptr;

        private:
            unsigned char binaryThreshold;
            Image result;
            Image mask;
        };

    } // namespace vision
} // namespace cook


#endif //ORIENTSCAN3D_CONTOUREXTRACTOR_H
