//
// Created by yildirim on 27.06.15.
//

#ifndef ORIENTSCAN3D_RECONSTRUCTION_H
#define ORIENTSCAN3D_RECONSTRUCTION_H

#include <projector_model.h>
#include <scan_data_graycode.h>
#include <correspondence.h>
#include <calibration_single.h>
#include <camera_model.h>
#include <projector_model.h>
#include <utilities.h>
#include <types_vision.h>

namespace cook {
	namespace vision {

        struct ScanBoundary
        {
            float minX;
            float maxX;
            float minY;
            float maxY;
            float minZ;
            float maxZ;

            ScanBoundary()
            {
                setDefaults();
            }

            void setDefaults()
            {
                minX = -1000.f;
                maxX = 1000.f;
                minY = -1000.f;
                maxY = 1000.f;
                minZ = -100.f;
                maxZ = 100.f;
            }
        };

		bool Estimate3DPoint( const double& U,
			const double& V,
			const double&W,
			CameraModel& camera,
			ProjectorModel& projector,
			Point3f& p3f);

		bool CalculateCloudFromGrayCodeScanData(CorrespondenceAbsolute abs_corr, 
			ScanDataGrayCode::Ptr scan_data, 
			const Image& abs_phase, 
			std::vector<cv::Point3f>& points, 
			std::vector<cv::Scalar>& color,
			ScanBoundary limits);

		//triangulation comes here

	} // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_RECONSTRUCTION_H
