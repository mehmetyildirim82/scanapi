#ifndef ORIENTSCAN3D_TEMPLATE_BASED_MARKER_DETECTOR_H
#define ORIENTSCAN3D_TEMPLATE_BASED_MARKER_DETECTOR_H

#include <marker_detector.h>
#include <iostream>

namespace cook
{
	namespace vision
	{
		class MarkerDetectorTemplateMatching :	public MarkerDetector
		{
		public:
			bool Detect(const Image& in, MarkerArray2d& out, Image* outImg=NULL);
			void SetTemplates(std::vector<cook::Image> templates);
		private:
			std::vector<cook::Image> templates;
		};
	}
}

#endif