//
// Created by yildirim on 28.08.15.
//

#ifndef ORIENTSCAN3D_MERKER_DETECTOR_CHECKERBOARD_CORNER_H
#define ORIENTSCAN3D_MERKER_DETECTOR_CHECKERBOARD_CORNER_H

#include <marker_detector.h>

namespace cook{
    namespace vision{
        class MarkerDetectorCheckerboard : public MarkerDetector{
        public:
            MarkerDetectorCheckerboard() :
                    //Default Values
                    maxPt(250),
                    minDist(10)
            {};

            bool Detect(const Image& in, MarkerArray2d& out, Image* outImg=NULL);
            void SetMinDistance(int minDist);
            void SetMaxPoint(int maxPt);
        private:
            int maxPt;
            int minDist;
        };
    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_MERKER_DETECTOR_CHECKERBOARD_CORNER_H
