//
// Created by yildirim on 27.06.15.
//

#ifndef ORIENTSCAN3D_GRAYCODE_H
#define ORIENTSCAN3D_GRAYCODE_H

#include <types_vision.h>
#include <scan_data_graycode.h>

namespace cook {
    namespace vision{

        bool CreatePhasePattern(Image& img, short layer, cv::Scalar color = cv::Scalar(255,255,255));

        //Create gray code patterns dynamically
        bool CreateGrayCodePattern(Image& img, short layer, bool invers = false, cv::Scalar color = cv::Scalar(255,255,255));

        bool IsBitSet( unsigned long code, short bit );

        unsigned long GrayCode(unsigned long n);

        unsigned long Phase(unsigned long n);

        //TODO: Documentation!
        bool CalculateGrayCodeImage(const ImageSequence& sequence, Image& graycode);

        //TODO: Documentation!
        bool CalculatePhaseImage(const ImageSequence& sequence, Image& absolutePhase);

        //TODO: Documentation!
        bool GenerateMaskFromPhase(const ImageSequence& phaseIn, int DynamicThreshold, int MaximumThreshold, int SinusThreshold, Image& maskOut);

        //TODO: Documentation!
        bool GenerateAbsolutePhase(const Image& phase, const Image& graycode, Image& absPhase);

        bool CreateAbsolutePhaseImageFromScanData(ScanDataGrayCode::Ptr scanData, Image& absPhaseImage, int mask_parameter = 15);

        //TODO: Documentation!
        bool LoadAbsolutePhase(const char* filename, Image& absPhase);

        //TODO: Documentation!
        unsigned long LinearCode(unsigned long n);

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_GRAYCODE_H
