//
// Created by yildirim on 13.06.15.
//

#ifndef ORIENTSCAN3D_ID_MARKER_H
#define ORIENTSCAN3D_ID_MARKER_H

#include <marker.h>

namespace cook {
    namespace vision {

        //TODO: Documentation
        class IdMarker : public Marker {
        public:
            IdMarker():
                    //Default Parameters
                    radialOffset(2),
                    radialStepAmount(72)
            {};

            virtual bool FindSelf(const ContourArray& contours, MarkerArray2d& markers, Image* mask=NULL, Image* outImg=NULL) const;

            typedef std::shared_ptr<IdMarker> Ptr;

            unsigned int& GetRadialOffset(){return radialOffset;}
            unsigned int& GetStepAmount(){return radialStepAmount;}

            static const auto MIN_TEETH_MARKER = 2;
            static const auto MAX_TEETH_MARKER = 12;

        private:
            unsigned int radialOffset;
            unsigned int radialStepAmount;

            int GetTeethAmount(const Image& mask, const Point2f& center, const int radius, const int radialStepAmount, const int radialOffset) const;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_ID_MARKER_H
