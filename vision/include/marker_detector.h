//
// Created by yildirim on 28.08.15.
//

#ifndef ORIENTSCAN3D_MARKER_DETECTOR_H
#define ORIENTSCAN3D_MARKER_DETECTOR_H

#include <marker.h>

namespace cook{
    namespace vision{
        class MarkerDetector{
            virtual bool Detect(const Image& in, MarkerArray2d& out, Image* outImg=NULL) = 0;
        };
    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_MARKER_DETECTOR_H
