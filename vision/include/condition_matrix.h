//
// Created by yildirim on 02.06.15.
//

#ifndef ORIENTSCAN3D_MATRIX_TRANSFORMATIONS_H
#define ORIENTSCAN3D_MATRIX_TRANSFORMATIONS_H

#include <mat.h>
#include <exceptions.h>
#include <conversion.h>
#include <iostream>

namespace cook{
    namespace vision{

        /*!
         * @brief ConditionMatrix Class. Maps a given point array to coordinates between [-1.0, 1.0]
         * @class ConditionMatrix condition_matrix.h
         * @author Mehmet Yildirim
         * \ingroup Transformations
         * @{
         */
        template <typename T>
        class ConditionMatrix : public Mat {
        public:
            /*!
             * \brief ConditionMatrix overloaded constructor for point arrays
             * \param ...
             */
            ConditionMatrix(const std::vector<T>& in); /* throw exceptionOpenCv */

            //TODO: documentation
            bool Condition(const std::vector<T>& in, std::vector<T>& out) const;

        private:
            ///Calculates conditioning matrix for given point set
            void CalculateConditioning(const std::vector<T>& p2d); /* throw exceptionOpenCv */

        }; // class ConditionMatrix
        /*! @}*/
    } // namespace vision
} //namespace cook


namespace cook{
    namespace vision{

        template<>
        ConditionMatrix<Point2f>::ConditionMatrix(const std::vector<Point2f>& in) : Mat( 3, 3 ) {
            try{
                CalculateConditioning(in);
            }
            catch(...){
                throw exceptionOpenCV();
            }
        }

        template<>
        ConditionMatrix<Point3f>::ConditionMatrix(const std::vector<Point3f>& in) : Mat( 4, 4 ) {
            try{
                CalculateConditioning(in);
            }
            catch(...){
                throw exceptionOpenCV();
            }
        }

        template<typename T >
        bool ConditionMatrix<T>::Condition(const std::vector<T>& in, std::vector<T>& out) const {
            if(in.empty())
                return false;

            if(!out.empty())
                return false;

            for(size_t i = 0; i < in.size(); i++) {
                Mat conditioned = HomogenizeAndMultiply<T>(in[i], mat);
                NormaliseHomogenous(conditioned);
                conditioned.DeleteLasRow();
                T t(conditioned.GetReadOnly());
                out.push_back(t);
            }
            return true;
        }

        template<typename T >
        void ConditionMatrix<T>::CalculateConditioning(const std::vector<T>& in) {
            try {
                cv::Mat local_p(in);
                cv::Mat copy = local_p.clone();

                //Take mean of all points
                cv::Scalar t = cv::mean(copy);

                int size = copy.channels();

				float* fData = (float*)(copy.data);

                //Translate to origin using mean values
                for (int i = 0; i < in.size(); i++) {
                    for(int j = 0; j < size; j++) {
                        int index = i*size + j;
						//cv::Vec<float, size> d = copy.at<cv::Vec<float, size> >(index);
                        //copy.at<float>(index) = abs(copy.at<float>(index) - static_cast<float>(t[j]));

						fData[index] = abs( fData[index] - static_cast<float>(t[j]) );
                    }
                }

                //Take mean of translated
                cv::Scalar s = cv::mean(copy);

                //Conditioning using mean values
                cv::Mat S = cv::Mat::eye(size+1, size+1, CV_32F);
                for(int i = 0; i < size; i++) {
                    S.at<float>(cv::Point2i(i, i)) = static_cast<float>(1.0 / s[i]);
                }

                //Translation part of the matrix
                cv::Mat Tr = cv::Mat::eye(size+1, size+1, CV_32F);
                for(int i = 0; i < size; i++) {
                    Tr.at<float>(cv::Point2i(size, i)) = static_cast<float>(-t[i]);
                }

                cv::Mat temp = S * Tr;
                temp.convertTo(mat, CV_64F);
            }
            catch (...) {
                throw exceptionOpenCV();
            }
        }

    } // namespace vision
} // namespace cook



#endif //ORIENTSCAN3D_MATRIX_TRANSFORMATIONS_H
