//
// Created by yildirim on 21.06.15.
//

#ifndef ORIENTSCAN3D_STEREO_MODEL_H
#define ORIENTSCAN3D_STEREO_MODEL_H

#include <fstream>

#include <camera_model.h>

namespace cook {
    namespace vision {

        class StereoModel {
        public:

            enum CameraIndex {
                Camera1 = 0,
                Camera2
            };

            void SetCamera(CameraModel& camera, CameraIndex idx);
            CameraModel& GetCamera(CameraIndex idx);

            Mat& GetRotation();
            Mat& GetTranslation();
            Mat& GetEssentialMatrix();
            Mat& GetFundamentalMatrix();

            void DumpModel(std::ofstream& file);

        private:
            Mat rotation;
            Mat translation;
            Mat essentialMatrix;
            Mat fundamentalMatrix;

            static const int camAmount = 2;
            CameraModel cam[camAmount];
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_STEREO_MODEL_H
