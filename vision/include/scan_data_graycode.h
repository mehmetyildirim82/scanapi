//
// Created by yildirim on 27.06.15.
//

#ifndef ORIENTSCAN3D_SCAN_DATA_GRAYCODE_H
#define ORIENTSCAN3D_SCAN_DATA_GRAYCODE_H

#include <scan_data.h>
#include <basic_types.h>

namespace cook {
    namespace vision{

        //TODO: Documentation!
        class ScanDataGrayCode : public ScanData {
        public:
            enum DataSection {
                TEXTURE = 0,
                GRAYCODE,
                PHASESHIFT
            };

            ScanDataGrayCode();

            bool AddImage(Image scan_image);

            ImageSequence& GetDataSection(DataSection section);

        protected:
            void PreProcess(Image& scan_image);

		public:
			typedef std::shared_ptr<ScanDataGrayCode> Ptr;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_SCAN_DATA_GRAYCODE_H
