//
// Created by yildirim on 08.06.15.
//

#ifndef ORIENTSCAN3D_RING_MARKER_H
#define ORIENTSCAN3D_RING_MARKER_H

#include <marker.h>

namespace cook {
    namespace vision {

        //TODO: Documentation
        class RingMarker : public Marker {
        public:
            RingMarker();
            ~RingMarker();
            virtual bool FindSelf(const ContourArray& contours, MarkerArray2d& markers, Image* mask=NULL, Image* outImg=NULL) const;

            typedef std::shared_ptr<RingMarker> Ptr;
        };

    } // namespace vision
} // namespace cook


#endif //ORIENTSCAN3D_RING_MARKER_H
