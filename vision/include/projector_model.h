//
// Created by yildirim on 27.06.15.
//

#ifndef ORIENTSCAN3D_PROJECTOR_MODEL_H
#define ORIENTSCAN3D_PROJECTOR_MODEL_H

#include <mat.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        class ProjectorModel {
        public:
            ProjectorModel();
            Mat& GetModel();
        private:
            Mat model;
        };
    } //namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_PROJECTOR_MODEL_H
