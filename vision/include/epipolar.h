//
// Created by yildirim on 05.07.15.
//

#ifndef ORIENTSCAN3D_EPIPOLAR_H
#define ORIENTSCAN3D_EPIPOLAR_H

#include <types_vision.h>
#include <mat.h>
#include <projection_matrix.h>
#include <geometric.h>

namespace cook {
    namespace vision {

        //ax + by + c = 0
        typedef Point3f EpipolarLine;
        typedef std::vector<EpipolarLine> EpipolarBundle;

        //TODO: Documentation!
        bool ComputeEpipolarLines(const Point2dArray& points, const Mat& fundamentalMatrix, EpipolarBundle& bundle);

        /*
        bool DetectPointOnEpipolarLine(EpipolarLine& epipole,
                                       double absValueSearch,
                                       Image& targetAbsImage,
                                       const Point2f& ptSource,
                                       Point2f& ptCorresponding,
                                       ProjectionMatrix& P);
                                       */

        bool ReduceEpipolarSearchSpace(const Point2f& ptEstimate, float limit, Line2d& l);

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_EPIPOLAR_H
