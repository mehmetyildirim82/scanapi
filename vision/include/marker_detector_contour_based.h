//
// Created by yildirim on 08.06.15.
//

#ifndef ORIENTSCAN3D_CONTOUR_BASED_MARKER_DETECTOR_H
#define ORIENTSCAN3D_CONTOUR_BASED_MARKER_DETECTOR_H

#include <contour_extractor.h>
#include <marker_detector.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        class MarkerDetectorContourBased : public MarkerDetector{
        public:
            MarkerDetectorContourBased(){};
            void SetMarker(Marker::Ptr marker);
            void SetContourExtractor(ContourExtractor::Ptr extractor);
            const ContourExtractor::Ptr GetContourExtractor() const;

            bool Detect(const Image& in, MarkerArray2d& out, Image* outImg=NULL);

        private:
            Marker::Ptr marker;
            ContourExtractor::Ptr contourExtractor;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CONTOUR_BASED_MARKER_DETECTOR_H
