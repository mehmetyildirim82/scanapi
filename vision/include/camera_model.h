//
// Created by yildirim on 20.06.15.
//

#ifndef ORIENTSCAN3D_CAMERA_MODEL_H
#define ORIENTSCAN3D_CAMERA_MODEL_H

#include <fstream>

#include <projection_matrix.h>
#include <types_vision.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        class CameraModel {
        public:
            CameraModel();
            bool Init(ProjectionMatrix& P);

            IntrinsicMat&       GetIntrinsics();
            Mat&                GetRotation();
            Mat&                GetProjectionCenter();
            DistortionCoeff&    GetDistortionCoeff();
            double&             GetAccuracy();
            ProjectionMatrix&   GetProjectionMat();

            bool ProjectPoints(const Point3dArray& p3dArray, Point2dArray& p2dArray, bool doUndistortion = false)const;
            float TestProjection(const Point3f& p3f, const Point2f& p2f, bool doUndistortion = false)const;

            void DumpModel(std::ofstream& file);

            void Recompose();
            void Decompose();

            ImageSize& GetImageSize();

        private:
            Mat rotation;
            Mat projectionCenter;
            IntrinsicMat intrinsics;
            ProjectionMatrix projection;
            DistortionCoeff distortionCoeff;

            ImageSize size;

            /// calibration accuracy
            double geometricError;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CAMERA_MODEL_H
