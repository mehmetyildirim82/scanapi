//
// Created by yildirim on 14.06.15.
//

#ifndef ORIENTSCAN3D_CORRESPONDENCE_MATCHER_H
#define ORIENTSCAN3D_CORRESPONDENCE_MATCHER_H

#include <correspondence.h>
#include <projection_matrix.h>
#include <epipolar.h>
#include <stereo_model.h>
#include <projector_model.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        class CorrespondenceMatcher {
        public:

            bool Match(const IdMarkerArray3d& idMarkerWorld,
                       const IdMarkerArray2d& idMarkerImage,
                       Correspondence3d& correspondence) const;

            bool Match(const MarkerArray3d& MarkerWorld,
                       const MarkerArray2d& MarkerImage,
                       Correspondence3d& correspondence,
                       const ProjectionMatrix& P,
                       Image* debug_img = NULL,
                       float search_radius = 1500.0f) const;

            bool Match(const Correspondence3d& cam01,
                       const Correspondence3d& cam02,
                       StereoCorrespondence& stereo) const;

            bool Match(const CorrespondenceAbsolute& cam01,
                       const CorrespondenceAbsolute& cam02,
                       StereoCorrespondence& stereo) const;

            bool Match(StereoModel& model,
                       ProjectorModel& projector,
                       const Image& img01,
                       const Image& img02,
                       const EpipolarBundle& epipolar,
                       Correspondence2d& corr) const;
        };

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CORRESPONDENCE_MATCHER_H
