//
// Created by yildirim on 28.06.15.
//

#ifndef ORIENTSCAN3D_INTERPOLATION_H
#define ORIENTSCAN3D_INTERPOLATION_H

#include <iostream>
#include <geometric.h>

namespace cook {
    namespace vision {

        template <typename T>
        double BilinearInterpolation(Point2f position, const Image& img) {
            double dx = position.x-(int)position.x;
            double dy = position.y-(int)position.y;

            T ptl = img.at<T>((int)position.y , (int)position.x);
            if(!ptl)dx=1.0;

            T ptr = img.at<T>((int)position.y, (int)position.x+1);
            if(!ptr)dx=0.0;

            T pbl = img.at<T>((int)position.y+1, (int)position.x);
            if(!pbl)dy=1.0;

            T pbr = img.at<T>((int)position.y+1, (int)position.x+1);
            if(!pbr)dy=0.0;

            double weight_tl = (1.0 - dx) * (dy);
            double weight_tr = (dx)       * (dy);
            double weight_bl = (1.0 - dx) * (1.0 - dy);
            double weight_br = (dx)       * (1.0 - dy);

            double result =(ptl*weight_tl)+(ptr*weight_tr)+(pbl*weight_bl)+(pbr*weight_br);
            return result;
        }

#if 0
double CScanCalibration::SampleAbsPhase(double x, double y, const CGImage &Phase)
{
  if( Phase.GetType() != PIX_16BIT )
      return 0.0;

  const long u0 = (long)x;
  const long u1 = u0 + 1;
  const long v0 = (long)y;
  const long v1 = v0 + 1;

  const short *pPixel = static_cast<const short*> (Phase.GetLine(v0));
  const double ph0 = static_cast<double>( pPixel[u0] );
  const double ph2 = static_cast<double>( pPixel[u1] );
 
  pPixel = static_cast<const short*> (Phase.GetLine(v1));
  const double ph1 = static_cast<double>( pPixel[u0] );
  const double ph3 = static_cast<double>( pPixel[u1] );

  if( ph0==0 || ph1==0 || ph2==0 || ph3==0 )
      return 0.0;

  const double u  = x-u0;
  const double v  = y-v0;

  return BLInterpolation(ph0, ph1, ph2, ph3, u, v);
}
#endif 

        template <typename T>
        bool InterpolateValueOnLine(const Image& img, const Line2d& line, const T lookupValue, Point2f& posOut) {
            T last_value = 0;

            int width = img.cols;
            int height = img.rows;

            double n = cv::norm(line.direction) * 2;
            Point2f direction;
            direction.x = line.direction.x / n;
            direction.y = line.direction.y / n;
            int step_amount = 80;

            Point2f increase;
            Point2f cursor;

            for(int i = 0; i < step_amount; i++) {
                increase = (i*direction);
                cursor = line.center + increase;

                if(cursor.x > width-1 || cursor.x < 1)
                    continue;

                if(cursor.y > height-1 || cursor.y < 1)
                    continue;

                T value_pt = BilinearInterpolation<T>(cursor, img);

                if(last_value <= lookupValue && lookupValue <= value_pt && last_value > 0) {
                    posOut = cursor;
                    return true;
                }
                last_value = value_pt;
            }
            return false;
        }

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_INTERPOLATION_H

