//
// Created by yildirim on 19.06.15.
//

#ifndef ORIENTSCAN3D_CALIB_INI_READER_H
#define ORIENTSCAN3D_CALIB_INI_READER_H

#include <calibration_rig.h>
#include <boost/property_tree/ptree.hpp>
#include <correspondence.h>

namespace cook {
    namespace vision {

        /*!
         * @brief CalibIniIO Class. Reads/Writes various calibration files.
         * @class CalibIniIO calib_ini_io.h
         * @author Mehmet Yildirim
         * \ingroup I/O
         * @{
         */
        class CalibIniIO {
        public:

            /*!
             * \brief Reads calibration rig definition from the given file
             * \param rig Calibration rig structure
             * \return true if successfull
             */
            bool Read(CalibrationRig& rig)const; //TODO: Throws Exception??

            /*!
             * \brief Reads a raw calibration ini file into correspondences between world and image coordinates
             * \param correspondence converts file content to correspondence data strcuture
             * \return true if successfull
             */
            bool Read(CorrespondenceAbsolute& correspondence)const; //TODO: Throws Exception??

            /*!
             * \brief Generates a raw calibration ini file from given correspondences
             * \param filename
             * \param abs 16-bit absolute phase image
             * \param corr Correspondences between the world and image coordinates
             * \return true if successfull
             */
            bool Write(const char* filename, const Image& abs, CorrespondenceAbsolute& corr)const; //TODO: Throws Exception??

            /// Checks if a valid file has been set
            bool isValid() const {
                return this->fileValid;
            }

            /// Sets file to read and write
            bool SetFile(const char* filename);

        private:
            bool Validate(const char *filename);

            bool fileValid;
            boost::property_tree::ptree pt;
        };
        /*! @}*/

    } // namespace vision
} // namespace cook

#endif //ORIENTSCAN3D_CALIB_INI_READER_H
