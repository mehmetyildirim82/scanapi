//
// Created by yildirim on 14.06.15.
//

#ifndef ORIENTSCAN3D_VISION_STRUCTURES_H
#define ORIENTSCAN3D_VISION_STRUCTURES_H

#include <types_vision.h>

namespace cook {
    namespace vision {

        //TODO: Documentation!
        template <typename T>
        struct MarkerArray {
            std::vector<T> data;
            virtual ~MarkerArray(){};
        };
        typedef MarkerArray<Point2f> MarkerArray2d;
        typedef MarkerArray<Point3f> MarkerArray3d;

        template <typename T>
        struct IdMarkerArray : public MarkerArray<T>{
			std::vector<unsigned int> index;
            virtual ~IdMarkerArray(){};
            ///Returns point at origin in case it fails
            T GetMarkerbyIdx(unsigned int idx)const{
                for(int i=0;i<index.size();i++){
                    if(index[i]==idx)
                        return this->data[i];
                }
                return T();
            }
        };
        typedef IdMarkerArray<Point2f> IdMarkerArray2d;
        typedef IdMarkerArray<Point3f> IdMarkerArray3d;
    }
}

#endif //ORIENTSCAN3D_VISION_STRUCTURES_H