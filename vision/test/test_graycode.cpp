//
// Created by yildirim on 12.09.15.
//
#include <gtest/gtest.h>
#include <graycode.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace cook;
using namespace cook::vision;

TEST(GrayCode, TestDynamicGeneration) {
    Image img_graycode, img_abs;
    EXPECT_TRUE(CreateGrayCodePattern(img_graycode, 0, true, cv::Scalar(255,255,255)));
    EXPECT_TRUE(CreatePhasePattern(img_abs, 3, cv::Scalar(255,255,255)));

    cv::imwrite("pattern_graycode.bmp", img_graycode);
    cv::imwrite("pattern_abs_3.bmp", img_abs);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}