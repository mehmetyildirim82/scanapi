#include <gtest/gtest.h>
#include <dlt.h>
#include <exceptions.h>
#include <id_marker.h>
#include <correspondence_matcher.h>

void LoadGoldenStandard(cook::Point3dArray& p3d, cook::Point2dArray& p2d, cook::vision::ProjectionMatrix& P) {
    p3d.push_back(cook::Point3f(44.7f,-142.4f, 256.7f));
    p3d.push_back(cook::Point3f(-103.6f,-146.6f, 154.4f));
    p3d.push_back(cook::Point3f(47.4f, -150.1f, 59.8f));
    p3d.push_back(cook::Point3f(-152.2f, 59.4f, 245.2f));
    p3d.push_back(cook::Point3f(-153.3f, -96.9f, 151.3f));
    p3d.push_back(cook::Point3f(-149.4f, 52.7f, 46.9f));

    p2d.push_back(cook::Point2f(18.5, 46.8));
    p2d.push_back(cook::Point2f(99.1, 146.5));
    p2d.push_back(cook::Point2f(13.8, 221.8));
    p2d.push_back(cook::Point2f(242.1, 52.5));
    p2d.push_back(cook::Point2f(151.1, 147.1));
    p2d.push_back(cook::Point2f(243.1, 224.5));

    double data[12] = {  -0.6581, 0.5764, -0.004, 132.5556,
                        -0.1568, -0.1801, -0.9210, 270.3348,
                        -0.0006, -0.0005, 0.0001, 1.000};
    P = cook::vision::ProjectionMatrix(data);
}

TEST(ProjectionMatrix, InitializationArray) {
    double data[12] = {11,12,13,14,21,22,23,24,31,32,33};
    cook::vision::ProjectionMatrix projectionMatrix(data);
    EXPECT_EQ(23, projectionMatrix.At(1, 2));

    double data_short[5] = {11,12,13,14,15};
    EXPECT_NO_THROW({
                        cook::vision::ProjectionMatrix projectionMatrixShort(data_short);
                    });
    cook::vision::ProjectionMatrix projectionMatrixShort(data_short);
    EXPECT_EQ(13, projectionMatrixShort.At(0, 2));

    double data_long[15] = {11,12,13,14,21,22,23,24,31,32,33,34, 41, 42, 43};
    EXPECT_NO_THROW({
                        cook::vision::ProjectionMatrix projectionMatrixLong(data_long);
                    });
    cook::vision::ProjectionMatrix projectionMatrixLong(data_long);
    EXPECT_EQ(34, projectionMatrixLong.At(2, 3));

    //Assignment operator test
    cook::vision::ProjectionMatrix P = projectionMatrix;
    EXPECT_EQ(23, P.At(1, 2));
    EXPECT_EQ(P, projectionMatrix);
}

TEST(ProjectionMatrix, InitializationLinearMat) {
    double data[12] = {11,12,13,14,21,22,23,24,31,32,33,34};
    cook::Mat row(1,12,data);

    EXPECT_THROW({
                     cook::vision::ProjectionMatrix projectionMatrixRow(row);
                 }, cook::exceptionMatrixSize
    );
}

TEST(ProjectionMatrix, ProjectPoint) {
    cook::Point3dArray p3d;
    cook::Point2dArray p2d;
    cook::vision::ProjectionMatrix projectionMatrix;
    LoadGoldenStandard(p3d, p2d, projectionMatrix);

    EXPECT_FLOAT_EQ(-0.6581, projectionMatrix.At(0, 0));

    //Project first point
    cook::Point2f poi = projectionMatrix.Project(p3d[0]);
    cook::Point2f ref(18.5, 46.8);
    EXPECT_NEAR(ref.x, poi.x, 5.0);
    EXPECT_NEAR(ref.y, poi.y, 5.0);
}

TEST(DLTTest, Verification) {
    cook::Point3dArray p3d;
    cook::Point2dArray p2d;
    cook::vision::ProjectionMatrix projectionMatrixExpected;
    LoadGoldenStandard(p3d, p2d, projectionMatrixExpected);

    cook::vision::ProjectionMatrix projectionMatrixCalculated;
    //Calculate projection matrix
    EXPECT_TRUE(cook::vision::DLT(p3d, p2d, projectionMatrixCalculated));

    EXPECT_NEAR(projectionMatrixExpected.At(0,0), projectionMatrixCalculated.At(0,0), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(0,1), projectionMatrixCalculated.At(0,1), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(0,2), projectionMatrixCalculated.At(0,2), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(0,3), projectionMatrixCalculated.At(0,3), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(1,0), projectionMatrixCalculated.At(1,0), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(1,1), projectionMatrixCalculated.At(1,1), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(1,2), projectionMatrixCalculated.At(1,2), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(1,3), projectionMatrixCalculated.At(1,3), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(2,0), projectionMatrixCalculated.At(2,0), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(2,1), projectionMatrixCalculated.At(2,1), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(2,2), projectionMatrixCalculated.At(2,2), 1.01);
    EXPECT_NEAR(projectionMatrixExpected.At(2,3), projectionMatrixCalculated.At(2,3), 1.01);

    for(int i = 0; i < p3d.size(); i++) {
        cook::Point2f poi = projectionMatrixCalculated.Project(p3d[i]);
        cook::Point2f ref = p2d[i];
        EXPECT_NEAR(ref.x, poi.x, 0.5);
        EXPECT_NEAR(ref.y, poi.y, 0.5);
    }
}

TEST(DLTTest, ExceptionHandling) {
    cook::Point3dArray p3d;
    cook::Point2dArray p2d;
    cook::vision::ProjectionMatrix p;

    EXPECT_FALSE(cook::vision::DLT(p3d, p2d, p));

    p2d.push_back(cook::Point2f(1.0, 2.0));
    EXPECT_FALSE(cook::vision::DLT(p3d, p2d, p));
}

TEST(CameraModel, Initialization) {
    double data[12] = {-559.425, -69.134,    25.301,     -998.081,
                      -68.914,  -418.097,   355.103,    388.586,
                      -0.451,   0.211,      0.866,      -0.679};

    cook::vision::ProjectionMatrix P(data);
    EXPECT_NO_THROW({
                        cook::vision::CameraModel camera;
                        camera.Init(P);
                        EXPECT_NEAR(492.80, camera.GetIntrinsics().At(1,1), 0.5);
                        EXPECT_NEAR(250.61, camera.GetIntrinsics().At(1,2), 0.5);
                        EXPECT_NEAR(-0.204, camera.GetRotation().At(0,1), 0.5);
                        EXPECT_NEAR(-0.427, camera.GetProjectionCenter().At(2,0), 0.5);
                    });
}

TEST(CameraModel, ExtremeCases) {
    double data[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
    cook::vision::ProjectionMatrix P(data);

    cook::vision::CameraModel camera;
    EXPECT_FALSE(camera.Init(P));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}