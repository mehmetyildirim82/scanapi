#include <gtest/gtest.h>

#include <memory>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <utilities.h>
#include <contour_extractor.h>
#include <ring_marker.h>
#include "marker_detector_contour_based.h"
#include <marker_detector_checkerboard.h>
#include <id_marker.h>
#include <visualization.h>

//globals
std::vector<cv::Mat> images;

using namespace cook;
using namespace cook::vision;

void LoadSampleMarkerImages(std::vector<cv::Mat>& images) {
    images.push_back(cv::imread("resources/marker1.bmp"));
    images.push_back(cv::imread("resources/marker2.bmp"));
    images.push_back(cv::imread("resources/marker3.bmp"));
    images.push_back(cv::imread("resources/marker4.bmp"));
    images.push_back(cv::imread("resources/marker5.bmp"));
    images.push_back(cv::imread("resources/marker6.bmp"));
    images.push_back(cv::imread("resources/marker7.bmp"));
    images.push_back(cv::imread("resources/marker8.bmp"));
    images.push_back(cv::imread("resources/marker9.bmp"));
}

/// Test whether the samples are correctly loaded
TEST(MarkerDetection, LoadSampleImages)
{
    EXPECT_EQ(cv::Size(1296, 966), images[0].size());
    EXPECT_EQ(cv::Size(1296, 966), images[3].size());
    EXPECT_EQ(cv::Size(1296, 966), images[7].size());
    EXPECT_EQ(cv::Size(1296, 966), images[8].size());

    EXPECT_FALSE(cook::CheckFile("resources/osman.bmp"));
}

TEST(ContourExtractor, Verification) {
    cook::Image sample = cv::imread("resources/sample_marker.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    cook::vision::ContourExtractor::Ptr contourExtractor(new cook::vision::ContourExtractor());

    cook::vision::ContourArray contourArray;
    EXPECT_TRUE(contourExtractor->Extract(sample, contourArray));

    cook::Image result = contourExtractor->GetContourImage();
    EXPECT_EQ(sample.size(), result.size());

    // There are two contours in sample image
    EXPECT_EQ(2, contourArray.size());
}

TEST(MarkerDetectorContourBased, VerificationSimple) {
    
cook::Image sample = cv::imread("resources/sample_marker.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    cook::vision::MarkerArray2d p2d;
    cook::vision::ContourArray contourArray;

    cook::vision::RingMarker::Ptr ring_marker(new cook::vision::RingMarker());
    cook::vision::Marker::Ptr marker(ring_marker);

    cook::vision::ContourExtractor::Ptr contourExtractor(new cook::vision::ContourExtractor);
    contourExtractor->Extract(sample, contourArray);

    cook::vision::MarkerDetectorContourBased MarkerDetectorContourBased;
    MarkerDetectorContourBased.SetMarker(marker);
    MarkerDetectorContourBased.SetContourExtractor(contourExtractor);

    EXPECT_TRUE(MarkerDetectorContourBased.Detect(sample, p2d));
    EXPECT_EQ(2, p2d.data.size());
}

TEST(MarkerDetectorContourBased, VerificationComplex) {
    cook::vision::RingMarker::Ptr ring_marker(new cook::vision::RingMarker());
    ring_marker->GetMinSize() = cv::Size2f(8.0f, 8.0f);
    ring_marker->GetMaxSize() = cv::Size2f(20.0f, 20.0f);
    cook::vision::Marker::Ptr marker(ring_marker);

    cook::vision::ContourExtractor::Ptr contourExtractor(new cook::vision::ContourExtractor());
    contourExtractor->GetBinaryThreshold() = 30;

    cook::vision::MarkerDetectorContourBased MarkerDetectorContourBased;
    MarkerDetectorContourBased.SetMarker(marker);
    MarkerDetectorContourBased.SetContourExtractor(contourExtractor);

    cook::Image sample = cv::imread("resources/marker1.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    cook::vision::MarkerArray2d p2d;
    cook::Image outImg;
    EXPECT_TRUE(MarkerDetectorContourBased.Detect(sample, p2d, &outImg));
    cv::imwrite("marker_test_result.bmp", outImg);
    EXPECT_EQ(7, p2d.data.size());
}

TEST(MarkerDetectorContourBased, IdMarkerIdentification) {
    cook::vision::IdMarker::Ptr idmarker_algo(new cook::vision::IdMarker());
    idmarker_algo->GetMinSize() = cv::Size2f(30.0f, 30.0f);
    idmarker_algo->GetMaxSize() = cv::Size2f(120.0f, 120.0f);
    idmarker_algo->GetRadialOffset() = 2;
    idmarker_algo->GetStepAmount() = 72;
    cook::vision::ContourExtractor::Ptr ce(new cook::vision::ContourExtractor());
    ce->GetBinaryThreshold() = 40;
    cook::vision::MarkerDetectorContourBased detector;
    detector.SetContourExtractor(ce);
    detector.SetMarker(idmarker_algo);

    cook::Image sample = cv::imread("resources/marker2.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    cook::vision::IdMarkerArray2d idmarkers;
    EXPECT_TRUE(detector.Detect(sample, idmarkers));
    EXPECT_EQ(7, idmarkers.index.size());
}

TEST(CheckerboardMarkerDetector, DetectPointsOnCalibBody){
    cook::Image sample = cv::imread("resources/calibL.bmp", CV_LOAD_IMAGE_GRAYSCALE);

    if(!sample.empty()){
        MarkerArray2d markers;

        MarkerDetectorCheckerboard detector;
        detector.SetMinDistance(10);
        detector.SetMaxPoint(250);
        detector.Detect(sample, markers);

        for(int i = 0; i < markers.data.size(); i++)
        {
            cv::circle(sample, markers.data[i], 2, cv::Scalar(255,255,255));
        }
        cv::imwrite("harris.bmp", sample);
    }
}

int main(int argc, char **argv) {
    LoadSampleMarkerImages(images);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}