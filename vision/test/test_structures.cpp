#include <gtest/gtest.h>
#include <projection_matrix.h>
#include <exceptions.h>
#include <conversion.h>

TEST(VisionStructuresTest, MatExceptions) {
    EXPECT_THROW({
                     cook::Mat m(-1, -1);
                 },
                 cook::exceptionOpenCV );

    cook::Mat m(10, 10);
    //Excessive index returns undefined value, no exception!!
    EXPECT_NO_THROW({m.At(-1, -1);});
    //Excessive index returns undefined value, no exception!!!
    EXPECT_NO_THROW({m.At(15, 15);});

    cook::Mat p3d(cook::Point3f(3,2,1));
    EXPECT_FLOAT_EQ(3, p3d.At(0,0));
    EXPECT_FLOAT_EQ(2, p3d.At(1,0));
    EXPECT_FLOAT_EQ(1, p3d.At(2,0));
}

TEST(VisionStructuresTest, HomogenousCoordinates) {
    double data[4] = {3.0, 6.0, 9.0, 3.0};
    cook::Mat mat_homo(4, 1, data);
    EXPECT_FLOAT_EQ(3.0, mat_homo.At(0,0));
    EXPECT_FLOAT_EQ(6.0, mat_homo.At(1,0));
    EXPECT_FLOAT_EQ(9.0, mat_homo.At(2,0));
    EXPECT_FLOAT_EQ(3.0, mat_homo.At(3,0));

    cook::Mat mat_homo_test = mat_homo.Get() / 3;
    cook::vision::NormaliseHomogenous(mat_homo);
    EXPECT_EQ(mat_homo_test, mat_homo);
}

TEST(VisionStructuresTest, MatCopyAndAssignmet) {
    double data[5] = {1,2,3,4,5};
    cook::Mat foo(5,1, data);
    cook::Mat bar(foo);

    EXPECT_EQ(foo, bar);

    foo = bar.Get() * 2;
    EXPECT_FLOAT_EQ(2.0, foo.At(0,0));
    EXPECT_FLOAT_EQ(4.0, foo.At(1,0));
}

TEST(VisionStructuresTest, HomogenizeWithMultiplication) {
    cook::Point3f p3d(121.0, 234.0, 315.0);
    double data[12] = {1,3,4,5,4,2,5,2,4,5,6,7};
    cook::Mat to_multiply(3,4,data);
    cook::Mat p3d_homo_non_multiplied = cook::vision::Homogenize(p3d);
    EXPECT_EQ(p3d.x, p3d_homo_non_multiplied.At(0,0));
    EXPECT_EQ(p3d.y, p3d_homo_non_multiplied.At(1,0));
    EXPECT_EQ(p3d.z, p3d_homo_non_multiplied.At(2,0));
    EXPECT_EQ(1, p3d_homo_non_multiplied.At(3,0));
}

TEST(VisionStructuresTest, InitializiationFromCvMat) {
    double data[] = {1,2,3,4,5,6,7,8,9,10,11,12};
    cv::Mat mat(3,4,CV_64F, data);
    cook::Mat m(mat);
    EXPECT_EQ(mat, m);
    EXPECT_EQ(12, m.At(2,3));

    cook::vision::ProjectionMatrix P(m);
    EXPECT_EQ(12, P.At(2,3));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

