#include <gtest/gtest.h>
#include <opencv2/highgui/highgui.hpp>

#include <graycode.h>
#include <scan_data_graycode.h>
#include <interpolation.h>
#include <calib_ini_io.h>
#include <reconstruction.h>

using namespace cook::vision;
using namespace cook;

ScanDataGrayCode sequence;

void LoadImageSequence01(ScanDataGrayCode& sequence){
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_00.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_01.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_02.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_03.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_04.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_05.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_06.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_07.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_08.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_09.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_10.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_11.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_12.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_13.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_14.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_15.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_16.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_17.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_18.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_19.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_01_20.png"));
}

void LoadImageSequence02(ScanDataGrayCode& sequence){
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_00.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_01.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_02.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_03.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_04.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_05.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_06.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_07.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_08.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_09.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_10.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_11.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_12.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_13.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_14.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_15.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_16.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_17.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_18.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_19.png"));
    sequence.AddImage(cv::imread("resources/sequence/Img_native_02_20.png"));
}

TEST(AbsPhase, TestCalculation)
{
#ifdef RUN_ADVANCED_TESTS
    Image graycode;
    EXPECT_EQ(sequence.GetDataSection(ScanDataGrayCode::GRAYCODE).size(),16);
    EXPECT_TRUE(CalculateGrayCodeImage(sequence.GetDataSection(ScanDataGrayCode::GRAYCODE), graycode));
    cv::imwrite("graycode.bmp", graycode);

    Image phase;
    EXPECT_TRUE(CalculatePhaseImage(sequence.GetDataSection(ScanDataGrayCode::PHASESHIFT), phase));
    cv::imwrite("phase.bmp", phase);

    Image mask;
    GenerateMaskFromPhase(sequence.GetDataSection(ScanDataGrayCode::PHASESHIFT), 5, 255, 10, mask);
    cv::imwrite("mask.bmp", mask);

    Image phase_masked;
    phase.copyTo(phase_masked, mask);
    cv::imwrite("phase_masked.bmp", phase_masked);

    Image graycode_masked;
    graycode.copyTo(graycode_masked, mask);

    Image abs_phase;
    GenerateAbsolutePhase(phase_masked, graycode_masked, abs_phase);
    cv::imwrite("abs_phase02.tiff", abs_phase);

    Image loaded_abs;
    EXPECT_TRUE(LoadAbsolutePhase("abs_phase02.tiff", loaded_abs));

#endif
}

TEST(AbsPhase, TestInterpolation)
{
    CalibIniIO reader;
    reader.SetFile("resources/rawcal.ini");
    CorrespondenceAbsolute corr;
    reader.Read(corr);

    Image absL;
    LoadAbsolutePhase("resources/abs_phase.tiff", absL);

    Point3f p3f = corr.geometric.from[10];
    Point2f p2f = corr.geometric.to[10];
    double W = corr.absoluteValue[10];
    double Wi_s = BilinearInterpolation<short>(p2f, absL);
    EXPECT_FLOAT_EQ(W, Wi_s);
}

int main(int argc, char **argv) {
    LoadImageSequence02(sequence);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}