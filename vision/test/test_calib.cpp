#include <gtest/gtest.h>

#include <fstream>

#include <marker_data.h>
#include <calib_ini_io.h>
#include <projection_matrix.h>
#include <camera_model.h>
#include <calibration_single.h>
#include <reconstruction.h>
#include <dlt.h>
#include <conversion.h>
#include <interpolation.h>

using namespace cook::vision;
using namespace cook;

TEST(CalibIniFile, TestReader) {
    CalibrationRig rig;

    CalibIniIO calib_reader;
    calib_reader.SetFile("resources/Calib_Hosteomess112.ini");
    EXPECT_TRUE(calib_reader.isValid());
    EXPECT_TRUE(calib_reader.Read(rig));
    EXPECT_EQ(112, rig.countMarker);
    EXPECT_EQ(-74.5, rig.idMarkers.GetMarkerbyIdx(3).x);
    EXPECT_EQ(64.0, rig.idMarkers.GetMarkerbyIdx(5).y);
    EXPECT_EQ(25.0, rig.idMarkers.GetMarkerbyIdx(4).z);
}

TEST(RawCalibFiles, TestReader)
{
    CalibIniIO ini_reader;
    EXPECT_TRUE(ini_reader.SetFile("resources/RawCalibCamera01.ini"));
    CorrespondenceAbsolute corr;
    EXPECT_TRUE(ini_reader.Read(corr));
    EXPECT_EQ(2.466213E+004, corr.absoluteValue[0]);
    EXPECT_EQ(8.457521E+003, corr.absoluteValue[78]);
    EXPECT_EQ(-1.460000E+002, corr.geometric.from[0].x);
}

TEST(CalibParameters, TestSingleCam3dReconstruction) {
    #if 1
    CalibIniIO reader;
    reader.SetFile("resources/rawcal.ini");
    CorrespondenceAbsolute corr;
    reader.Read(corr);

    CameraModel absoluteCamera;
    ProjectorModel projector;

    CalibrationSingle calib;
    EXPECT_TRUE(calib.Calibrate(corr, absoluteCamera, projector));
/*
    EXPECT_NEAR(absoluteCamera.GetProjectionMat().At(0,0), 2.32456, 1.0);
    EXPECT_NEAR(absoluteCamera.GetProjectionMat().At(1,3), -2262.22, 1.0);
    EXPECT_NEAR(absoluteCamera.GetProjectionMat().At(2,3), 1, 1.0);
    EXPECT_NEAR(projector.GetModel().At(0,0), -32.1641, 1.0);
    EXPECT_NEAR(projector.GetModel().At(0,3), 14673.7, 1.0);
    EXPECT_NEAR(projector.GetModel().At(0,7), 1, 1.0);
*/

    double U, V, W, X_ref, Y_ref, Z_ref;
    X_ref = corr.geometric.from[0].x;
    Y_ref = corr.geometric.from[0].y;
    Z_ref = corr.geometric.from[0].z;
    U = corr.geometric.to[0].x;
    V = corr.geometric.to[0].y;
    W = corr.absoluteValue[0];
    Point3f p3f_est;

    ///3d reconstruction
    EXPECT_TRUE(Estimate3DPoint(U, V, W, absoluteCamera, projector, p3f_est));

    //reference vs estimated 3d
    EXPECT_NEAR(p3f_est.x, X_ref, 1.0);
    EXPECT_NEAR(p3f_est.y, Y_ref, 1.0);
    EXPECT_NEAR(p3f_est.z, Z_ref, 1.5);

    double abs = absoluteCamera.TestProjection(corr.geometric.from[20], corr.geometric.to[20]);
    EXPECT_NEAR(0, abs, 2.0);
    abs = absoluteCamera.TestProjection(corr.geometric.from[40], corr.geometric.to[40]);
    EXPECT_NEAR(0, abs, 2.0);
    abs = absoluteCamera.TestProjection(corr.geometric.from[70], corr.geometric.to[70]);
    EXPECT_NEAR(0, abs, 2.0);
    abs = absoluteCamera.TestProjection(corr.geometric.from[0], corr.geometric.to[0]);
    EXPECT_NEAR(0, abs, 2.0);

    #endif
}

TEST(CalibrationAccuracy, CompareTwoCalibrationMethods) {
#if 0
    CalibIniIO reader;
    reader.SetFile("resources/RawCalBigCalib.ini");
    CorrespondenceAbsolute corr;
    reader.Read(corr);

    CameraModel cameraAbsolute;
    ProjectorModel projector;
    //CameraModel cameraConventional;

    CalibrationSingle calib;

    EXPECT_TRUE(calib.Calibrate(corr, cameraAbsolute, projector));
    std::cout << cameraAbsolute.GetIntrinsics().Get() << std::endl;

    /*
    ProjectionMatrix P;
    Point3dArray p3;
    Point2dArray p2;
    p3.push_back(corr.geometric.from[0]);
    p2.push_back(corr.geometric.to[0]);
    p3.push_back(corr.geometric.from[20]);
    p2.push_back(corr.geometric.to[20]);
    p3.push_back(corr.geometric.from[40]);
    p2.push_back(corr.geometric.to[40]);
    p3.push_back(corr.geometric.from[60]);
    p2.push_back(corr.geometric.to[60]);
    p3.push_back(corr.geometric.from[80]);
    p2.push_back(corr.geometric.to[80]);
    p3.push_back(corr.geometric.from[100]);
    p2.push_back(corr.geometric.to[100]);
    DLT(p3, p2, P);
    cameraConventional.Init(P);
    cameraConventional.GetImageSize() = cv::Size(1296, 966);
    EXPECT_NEAR(0.0, calib.Calibrate(corr.geometric, cameraConventional),5.0);

TestProjection
    //std::ofstream fileabs("absolute.txt");
    //std::ofstream fileconv("conv.txt");
     */

    double abs = cameraAbsolute.TestProjection(corr.geometric.from[20], corr.geometric.to[20]);
    //double con = cameraConventional.TestProjection(corr.geometric.from[20], corr.geometric.to[20]);
    EXPECT_NEAR(0, abs, 1.0);

    //Point3dArray test3d;
    //test3d.push_back(Point3f(0,0,1000));
    //Point2dArray test2d_con, test2d_abs;
    //cameraConventional.ProjectPoints(test3d, test2d_con);
    //cameraAbsolute.ProjectPoints(test3d, test2d_abs);
    //EXPECT_NEAR(test2d_con[0].x, test2d_abs[0].x, 0.1);
    //EXPECT_NEAR(test2d_con[0].y, test2d_abs[0].y, 0.1);

    //cameraAbsolute.DumpModel(fileabs);
    //cameraConventional.DumpModel(fileconv);
#endif
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
