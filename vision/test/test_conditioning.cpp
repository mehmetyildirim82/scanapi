#include <gtest/gtest.h>
#include <condition_matrix.h>

//from CV&RS Lecture 6 example
void LoadGoldStandard(cook::Point2dArray& p2d) {
    p2d.push_back(cook::Point2f(93.0f, 617.0f));
    p2d.push_back(cook::Point2f(729.0f, 742.0f));
    p2d.push_back(cook::Point2f(152.0f, 1103.0f));
    p2d.push_back(cook::Point2f(703.0f, 1233.0f));
}

void Load3DCube(cook::Point3dArray& p3d) {
    //This supposed to be a cube
    p3d.push_back(cook::Point3f(1000000.0f, 1000000.0f, 1000000.0f));
    p3d.push_back(cook::Point3f(2000000.0f, 2000000.0f, 2000000.0f));
    p3d.push_back(cook::Point3f(2000000.0f, 1000000.0f, 1000000.0f));
    p3d.push_back(cook::Point3f(2000000.0f, 1000000.0f, 2000000.0f));
    p3d.push_back(cook::Point3f(1000000.0f, 1000000.0f, 2000000.0f));
    p3d.push_back(cook::Point3f(1000000.0f, 2000000.0f, 2000000.0f));
    p3d.push_back(cook::Point3f(1000000.0f, 2000000.0f, 1000000.0f));
    p3d.push_back(cook::Point3f(2000000.0f, 2000000.0f, 1000000.0f));
}

TEST(ConditioningTest, ResultVerification2d) {
    cook::Point2dArray p2d;
    LoadGoldStandard(p2d);

    cook::vision::ConditionMatrix<cook::Point2f> conditionMatrix(p2d);
    EXPECT_NEAR(0.0034, conditionMatrix.At(0, 0), 0.1);
    EXPECT_NEAR(0.0041, conditionMatrix.At(1, 1), 0.1);
    EXPECT_NEAR(-1.4128, conditionMatrix.At(0, 2), 0.1);
    EXPECT_NEAR(-3.7820, conditionMatrix.At(1, 2), 0.1);
}

TEST(ConditioningTest, ResultVerification3d){
    cook::Point3dArray p3d;
    Load3DCube(p3d);

    cook::vision::ConditionMatrix<cook::Point3f> conditionMatrix(p3d);
    EXPECT_NEAR(1.0 / 500000.0, conditionMatrix.At(0,0), 0.1);
    EXPECT_NEAR(1.0 / 500000.0, conditionMatrix.At(1,1), 0.1);
    EXPECT_NEAR(-3, conditionMatrix.At(0,3), 0.1);
    EXPECT_NEAR(-3, conditionMatrix.At(1,3), 0.1);
    EXPECT_NEAR(-3, conditionMatrix.At(2,3), 0.1);

    cook::Mat point_homo = cook::vision::Homogenize<cook::Point3f>(p3d[0]);
    EXPECT_FLOAT_EQ(1000000.0f, p3d[0].x);
    EXPECT_FLOAT_EQ(1000000.0f, point_homo.At(0,0));
    EXPECT_FLOAT_EQ(1000000.0f, point_homo.At(1,0));
    EXPECT_FLOAT_EQ(1000000.0f, point_homo.At(2,0));
    EXPECT_FLOAT_EQ(1.0f, point_homo.At(3,0));

    cook::Mat c = conditionMatrix.Get() * point_homo.Get();
    EXPECT_FLOAT_EQ(-1.0f, c.At(0,0));
    EXPECT_FLOAT_EQ(-1.0f, c.At(1,0));
    EXPECT_FLOAT_EQ(-1.0f, c.At(2,0));
    EXPECT_FLOAT_EQ(1.0f, c.At(3,0));
}

TEST(ConditioningTest, InitializeEmpty) {

    EXPECT_NO_THROW({
                        cook::Point2dArray p2d;
                        cook::vision::ConditionMatrix<cook::Point2f> conditionMatrix(p2d);
                    });
}

TEST(ConditioningTest, InitializeBig) {
    EXPECT_NO_THROW({
                        cook::Point2dArray p2d(10000);
                        cook::vision::ConditionMatrix<cook::Point2f> conditionMatrix(p2d);
                    });
}

TEST(ConditioningTest, InitializeZeros) {
    cook::Point2dArray p2d;
    for(int i = 0; i < 100; i++) {
        p2d.push_back(cook::Point2f(0.0, 0.0));
    }

    //Divide by zero must not cause exceptions
    EXPECT_NO_THROW({cook::vision::ConditionMatrix<cook::Point2f> conditionMatrix(p2d);});
}

TEST(ConditionTest, ArrayConditioning) {
    cook::Point2dArray p2d;
    LoadGoldStandard(p2d);
    cook::vision::ConditionMatrix<cook::Point2f> conditionMatrix2d(p2d);
    cook::Point2dArray conditioned_2d_array;
    EXPECT_TRUE(conditionMatrix2d.Condition(p2d, conditioned_2d_array));

    cook::Point2f ref(-1.0994f, -1.2559f);
    EXPECT_NEAR( ref.x, conditioned_2d_array[0].x, 0.01);
    EXPECT_NEAR( ref.y, conditioned_2d_array[0].y, 0.01);

    //test empty arrays
    cook::Point2dArray p2d_empty_from;
    cook::Point2dArray p2d_empty_to;
    EXPECT_FALSE(conditionMatrix2d.Condition(p2d_empty_from, p2d_empty_to));
    EXPECT_FALSE(conditionMatrix2d.Condition(p2d_empty_from, p2d_empty_to));
    EXPECT_FALSE(conditionMatrix2d.Condition(p2d_empty_from, p2d_empty_to));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
