#include <gtest/gtest.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <calib_ini_io.h>
#include <stereo_model.h>
#include <reconstruction.h>
#include <calibration_single.h>
#include <calibration_stereo.h>
#include <projector_model.h>
#include <correspondence_matcher.h>
#include <graycode.h>
#include <visualization.h>
#include <utilities.h>
#include <epipolar.h>
#include <interpolation.h>
#include <geometric.h>

using namespace cook::vision;
using namespace cook;

TEST(Scan, PComposeDecompose) {
    double data[12] = {  -559.425f, -69.134f, 25.301f, -998.081f,
                        -68.914f, -418.097f, 355.103f, 388.586f,
                        -0.451f, 0.211f, 0.866f, -0.679f};
    ProjectionMatrix P(data);

    IntrinsicMat K;
    Mat rotation, translation;
    P.Decompose(K, rotation, translation);

    ProjectionMatrix composedP;
    composedP.Compose(K, rotation, translation);
    EXPECT_NEAR(composedP.At(0,0), P.At(0,0), 1.5);
    EXPECT_NEAR(composedP.At(0,1), P.At(0,1), 1.5);
    EXPECT_NEAR(composedP.At(0,2), P.At(0,2), 1.5);
    EXPECT_NEAR(composedP.At(0,3), P.At(0,3), 1.5);
    EXPECT_NEAR(composedP.At(1,0), P.At(1,0), 1.5);
    EXPECT_NEAR(composedP.At(1,1), P.At(1,1), 1.5);
    EXPECT_NEAR(composedP.At(1,2), P.At(1,2), 1.5);
    EXPECT_NEAR(composedP.At(1,3), P.At(1,3), 1.5);
    EXPECT_NEAR(composedP.At(2,0), P.At(2,0), 1.5);
    EXPECT_NEAR(composedP.At(2,1), P.At(2,1), 1.5);
    EXPECT_NEAR(composedP.At(2,2), P.At(2,2), 1.5);
    EXPECT_NEAR(composedP.At(2,3), P.At(2,3), 1.5);
}

TEST(Scan, VerifyPhaseShiftMethod)
{
    //Read correspondencies from ini files
    CorrespondenceAbsolute corrL;
    CorrespondenceAbsolute corrR;
    CalibIniIO reader;
    reader.SetFile("resources/rawcal.ini");
    reader.Read(corrL);
    reader.SetFile("resources/rawcal02.ini");
    reader.Read(corrR);

    //Calibrate each camera seperately
    StereoModel stereoModel;
    ProjectorModel projectorL;
    ProjectorModel projectorR;
    CalibrationSingle calib;
    calib.Calibrate(corrL, stereoModel.GetCamera(StereoModel::Camera1), projectorL);
    calib.Calibrate(corrR, stereoModel.GetCamera(StereoModel::Camera2), projectorR);

    Image sample = cv::imread("resources/abs_phase.tiff", CV_LOAD_IMAGE_GRAYSCALE);
    Image sample02 = cv::imread("resources/abs_phase02.tiff", CV_LOAD_IMAGE_GRAYSCALE);
    Point2dArray projected;
    stereoModel.GetCamera(StereoModel::Camera1).ProjectPoints(corrL.geometric.from, projected);
    Visualize(sample,projected, "abs_phase_points.bmp");

    //Find stereo correspondences
    StereoCorrespondence corr_stereo;
    CorrespondenceMatcher matcher;
    matcher.Match(corrL, corrR, corr_stereo);
    EXPECT_EQ(corr_stereo.image01.size(), corr_stereo.image02.size());

    //Calibrate stereo system
    CalibrationStereo calib_stereo;
    EXPECT_NEAR(0.0, calib_stereo.Calibrate(corr_stereo, stereoModel), 0.5);

    CameraModel& cam01 = stereoModel.GetCamera(StereoModel::Camera1);
    CameraModel& cam02 = stereoModel.GetCamera(StereoModel::Camera2);

    Image absL, absR;
    LoadAbsolutePhase("resources/abs_phase.tiff",absL);
    LoadAbsolutePhase("resources/abs_phase02.tiff",absR);

    //array of pixels in first image
    //F in stereo model to calculate epipolar lines (Point3f)
    EpipolarBundle epipolar;
    Correspondence2d stereoView;
    Generate2dGrid(Rect(0,0,1296,966), stereoView.from);
    EXPECT_TRUE(ComputeEpipolarLines(stereoView.from, stereoModel.GetFundamentalMatrix(), epipolar));

#ifdef RUN_ADVANCED_TESTS
#if 0
    CorrespondenceMatcher corr_matcher;
    EXPECT_TRUE(corr_matcher.Match(stereoModel, projectorL, absL, absR, epipolar, stereoView));
    //std::cout << stereoView.from[81*1296+808] << std::endl;
    //std::cout << stereoView.to[81*1296+808] << std::endl;

    cv::Mat pnts3D = cv::Mat(1,1,CV_64FC4);
    cv::Mat cam0pnts(1,1,CV_64FC2);
    cv::Mat cam1pnts(1,1,CV_64FC2);
    std::ofstream file("3d.asc");
    for(int i = 0; i < stereoView.to.size(); i++) {
        cam0pnts.at<cv::Point2d>(0) = cv::Point2d(stereoView.from[i].x, stereoView.from[i].y);
        cam1pnts.at<cv::Point2d>(0) = cv::Point2d(stereoView.to[i].x, stereoView.to[i].y);
        cv:triangulatePoints(  cam01.GetProjectionMat().Get(),
                               cam02.GetProjectionMat().Get(),
                               cam0pnts,
                               cam1pnts,
                               pnts3D);
        pnts3D = pnts3D / pnts3D.at<double>(3);

        if(abs(pnts3D.at<double>(2)) < 100)
            file << pnts3D.at<double>(0) << " " << pnts3D.at<double>(1) << " " << pnts3D.at<double>(2) << std::endl;
    }
#else
    std::ofstream file("3d_gc.asc");
    for(int i = 0; i < stereoView.from.size(); i++) {
        Point2f p = stereoView.from[i];
        short srcW = BilinearInterpolation<short>(p, absL);
        if(!srcW)continue;

        Point3f pt3f;
        Estimate3DPoint( p.x, p.y, srcW, cam01, projectorL, pt3f);

        if(abs(pt3f.z) < 100)
            file << pt3f.x << " " << pt3f.y << " " << pt3f.z << std::endl;
    }
#endif
#endif
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}