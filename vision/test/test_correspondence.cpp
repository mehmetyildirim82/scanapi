#include <gtest/gtest.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <correspondence_matcher.h>
#include <calib_ini_io.h>
#include <utilities.h>
#include <contour_extractor.h>
#include <ring_marker.h>
#include "marker_detector_contour_based.h"
#include <id_marker.h>
#include <visualization.h>
#include <dlt.h>
#include <calibration_single.h>
#include <calibration_stereo.h>
#include <graycode.h>
#include <interpolation.h>

void DetectMarkers(cook::Image& img,
                   cook::vision::IdMarkerArray2d& id_markers,
                   cook::vision::MarkerArray2d& ring_markers,
                   cv::Size2f min_size, cv::Size2f max_size,
                   int binary_threshold_id, int binary_threshold_ring)
{
    //Id marker algorithm
    cook::vision::IdMarker::Ptr idmarker_algo(new cook::vision::IdMarker());
    idmarker_algo->GetMinSize() = min_size;
    idmarker_algo->GetMaxSize() = max_size;
    idmarker_algo->GetRadialOffset() = 4;
    idmarker_algo->GetStepAmount() = 72;

    //Ring marker algorithm
    cook::vision::RingMarker::Ptr ring_marker_algo(new cook::vision::RingMarker());
    ring_marker_algo->GetMinSize() = min_size;
    ring_marker_algo->GetMaxSize() = max_size;

    //Contour extractor
    cook::vision::ContourExtractor::Ptr ce(new cook::vision::ContourExtractor());
    ce->GetBinaryThreshold() = binary_threshold_id;

    //Detector
    cook::vision::MarkerDetectorContourBased detector;
    detector.SetContourExtractor(ce);

    detector.SetMarker(idmarker_algo);
    detector.Detect(img, id_markers);
    //cook::vision::Visualize(img, id_markers, "test_id_visu.bmp");

    ce->GetBinaryThreshold() = binary_threshold_ring;
    detector.SetMarker(ring_marker_algo);
    detector.Detect(img, ring_markers);
    //cook::vision::Visualize(img, ring_markers.data, "test_ring_visu.bmp");
}

TEST(Correspondence, TestIdMarkerSorting) {
    cook::vision::IdMarkerArray2d idmarkers_image;
    idmarkers_image.index.push_back(3);
    idmarkers_image.data.push_back(cook::Point2f(18.5f, 46.8f)); //3
    idmarkers_image.index.push_back(8);
    idmarkers_image.data.push_back(cook::Point2f(99.1f, 146.5f)); //8
    idmarkers_image.index.push_back(12);
    idmarkers_image.data.push_back(cook::Point2f(13.8f, 221.8f)); //12
    idmarkers_image.index.push_back(10);
    idmarkers_image.data.push_back(cook::Point2f(242.1f, 52.5f)); //10
    idmarkers_image.index.push_back(4);
    idmarkers_image.data.push_back(cook::Point2f(151.1f, 147.1f)); //4
    idmarkers_image.index.push_back(7);
    idmarkers_image.data.push_back(cook::Point2f(243.1f, 224.5f)); //7

    cook::vision::IdMarkerArray3d idmarkers_world;
    idmarkers_world.index.push_back(12);
    idmarkers_world.data.push_back(cook::Point3f(47.4f, -150.1f, 59.8f)); //12
    idmarkers_world.index.push_back(7);
    idmarkers_world.data.push_back(cook::Point3f(-149.4f, 52.7f, 46.9f)); //7
    idmarkers_world.index.push_back(3);
    idmarkers_world.data.push_back(cook::Point3f(44.7f, -142.4f, 256.7f)); //3
    idmarkers_world.index.push_back(4);
    idmarkers_world.data.push_back(cook::Point3f(-153.3f, -96.9f, 151.3f)); //4
    idmarkers_world.index.push_back(10);
    idmarkers_world.data.push_back(cook::Point3f(-152.2f, 59.4f, 245.2f)); //10
    idmarkers_world.index.push_back(8);
    idmarkers_world.data.push_back(cook::Point3f(-103.6f, -146.6f, 154.4f)); //8

    cook::vision::CorrespondenceMatcher corresponder;
    cook::vision::Correspondence3d correspondence;
    EXPECT_TRUE(corresponder.Match(idmarkers_world, idmarkers_image, correspondence));

    EXPECT_EQ(cook::Point3f(47.4f, -150.1f, 59.8f), correspondence.from[0]);
    EXPECT_EQ(cook::Point2f(13.8f, 221.8f), correspondence.to[0]);

    EXPECT_EQ(cook::Point3f(-152.2f, 59.4f, 245.2f), correspondence.from[4]);
    EXPECT_EQ(cook::Point2f(242.1f, 52.5f), correspondence.to[4]);
}

TEST(Correspondence, TestRingMarkerCorrespondence) {
#ifdef RUN_ADVANCED_TESTS
    cook::vision::IdMarkerArray2d idmarkers;
    cook::vision::MarkerArray2d ring_markers;
    cook::Image sample = cv::imread("resources/marker3.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    DetectMarkers(sample,idmarkers, ring_markers, cv::Size(30,30), cv::Size(120,120), 45, 40);

    cook::vision::CalibIniIO reader;
    cook::vision::CalibrationRig rig;
    reader.SetFile("resources/Calib_Hosteomess112.ini");
    reader.Read(rig);

    cook::vision::CorrespondenceMatcher corresponder;
    cook::vision::Correspondence3d id_sorted;
    corresponder.Match(rig.idMarkers, idmarkers, id_sorted);

    cook::vision::ProjectionMatrix P;
    cook::vision::DLT(id_sorted.from, id_sorted.to, P);

    float RMS = P.TestProjection(id_sorted.from[0], id_sorted.to[0]); //root mean square error
    EXPECT_NEAR(0.0f, RMS, 1.0f); // it is around 0.88 pixel

    cook::vision::Correspondence3d ring_correspondence;
    EXPECT_TRUE(corresponder.Match(rig.ringMarkers, ring_markers, ring_correspondence, P, NULL, 1500.0f));
    //cv::imwrite("test_corr_visu.bmp", sample);

    EXPECT_EQ(ring_correspondence.from.size(), ring_correspondence.to.size());

    cook::vision::CalibrationSingle calibrationSingle;
    cook::vision::CameraModel camera_model;
    camera_model.Init(P);
    camera_model.GetImageSize() = cook::ImageSize(1296,966);
    RMS = calibrationSingle.Calibrate(ring_correspondence, camera_model );
    EXPECT_NEAR(0.0f, camera_model.GetAccuracy(), 0.15f);

    cook::Point2dArray projected_points;
    camera_model.ProjectPoints(ring_correspondence.from, projected_points);
    cook::vision::Visualize(sample, projected_points, "test_refined.bmp");
#endif
}

TEST(Correspondence, TestStereoCorrespondence) {
#ifdef RUN_ADVANCED_TESTS
    cook::vision::IdMarkerArray2d idmarkers[2];
    cook::vision::MarkerArray2d ring_markers[2];
    cook::Image sample[2];
    sample[0] = cv::imread("resources/marker3.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    sample[1] = cv::imread("resources/marker4.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    DetectMarkers(sample[0],idmarkers[0], ring_markers[0], cv::Size(30,30), cv::Size(120,120), 45, 40);
    DetectMarkers(sample[1],idmarkers[1], ring_markers[1], cv::Size(40,40), cv::Size(60,60), 35, 40);

    cook::vision::Visualize(sample[0], ring_markers[0].data, "sampleL.bmp");
    cook::vision::Visualize(sample[1], ring_markers[1].data, "sampleR.bmp");

    cook::vision::CalibIniIO reader;
    reader.SetFile("resources/Calib_Hosteomess112.ini");
    cook::vision::CalibrationRig rig;
    reader.Read(rig);

    cook::vision::CorrespondenceMatcher corresponder;
    cook::vision::Correspondence3d id_sorted[2];
    cook::vision::Correspondence3d ring_correspondence[2];
    cook::vision::CameraModel camera_model[2];

    cook::vision::ProjectionMatrix P[2];
    for(int i = 0; i < 2; i++) {
        camera_model[i].GetImageSize() = sample[i].size();
        corresponder.Match(rig.idMarkers, idmarkers[i], id_sorted[i]);
        cook::vision::DLT(id_sorted[i].from, id_sorted[i].to, P[i]);

        float RMS = P[i].TestProjection(id_sorted[i].from[0], id_sorted[i].to[0]); //root mean square error
        std::cout << RMS << std::endl;
        EXPECT_NEAR(0.0f, RMS, 1.0f); // it is around 0.88 pixel

        camera_model[i].Init(P[i]);

        EXPECT_TRUE(corresponder.Match(rig.ringMarkers, ring_markers[i], ring_correspondence[i], P[i], &sample[i], 1000.0f));
        //cv::imwrite("stuff.bmp", sample[i]);
        EXPECT_EQ(ring_correspondence[i].from.size(), ring_correspondence[i].to.size());

        cook::vision::CalibrationSingle calibrationSingle;
        RMS = calibrationSingle.Calibrate(ring_correspondence[i], camera_model[i] );
        EXPECT_NEAR(0.0f, RMS, 0.2f);

        RMS = camera_model[i].TestProjection(ring_correspondence[i].from[0], ring_correspondence[i].to[0], true);
        EXPECT_NEAR(0.0f, RMS, 5.0f);
    }

    sample[0] = cv::imread("resources/marker3.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    sample[1] = cv::imread("resources/marker4.bmp", CV_LOAD_IMAGE_GRAYSCALE);
    cook::Point2dArray projected_points[2];
    camera_model[0].ProjectPoints(ring_correspondence[0].from, projected_points[0], true);
    camera_model[1].ProjectPoints(ring_correspondence[1].from, projected_points[1], true);
    cook::vision::Visualize(sample[0], projected_points[0], "sampleL_sonra.bmp");
    cook::vision::Visualize(sample[1], projected_points[1], "sampleR_sonra.bmp");

    cook::vision::StereoCorrespondence corr;
    cook::vision::CorrespondenceMatcher matcher;
    EXPECT_TRUE(matcher.Match(ring_correspondence[0], ring_correspondence[1], corr));
    EXPECT_EQ(corr.image01.size(), corr.image02.size());
    EXPECT_GT(corr.image02.size(), 0);

    cook::vision::StereoModel stereo;
    stereo.SetCamera(camera_model[0],cook::vision::StereoModel::Camera1);
    stereo.SetCamera(camera_model[1],cook::vision::StereoModel::Camera2);
    EXPECT_NEAR(0, stereo.GetCamera(cook::vision::StereoModel::Camera1).GetAccuracy(), 0.2);
    EXPECT_NEAR(0, stereo.GetCamera(cook::vision::StereoModel::Camera2).GetAccuracy(), 0.2);

    cook::vision::CalibrationStereo stereo_calib;
    EXPECT_LE(stereo_calib.Calibrate(corr, stereo), 0.15);
    std::ofstream file("stereo.txt");
    stereo.DumpModel(file);

    cook::Image loaded_abs;
    cook::vision::LoadAbsolutePhase("resources/abs_phase.tiff", loaded_abs);
    cook::Image loaded_abs02;
    cook::vision::LoadAbsolutePhase("resources/abs_phase02.tiff", loaded_abs02);

    cook::vision::CalibIniIO writer;

    cook::vision::CorrespondenceAbsolute abs_corrL(loaded_abs, ring_correspondence[0]);
    cook::vision::CorrespondenceAbsolute abs_corrR(loaded_abs, ring_correspondence[1]);

    writer.Write("rawcal.ini", loaded_abs, abs_corrL);
    writer.Write("rawcal02.ini", loaded_abs, abs_corrR);
#endif
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}