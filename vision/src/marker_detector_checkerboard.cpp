//
// Created by yildirim on 28.08.15.
//

#include "marker_detector_checkerboard.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>

namespace cook{
    namespace vision{

        bool MarkerDetectorCheckerboard::Detect(const Image& in, MarkerArray2d& out, Image* outImg){

            std::vector<cv::Mat> images;
            images.resize(1);
            cv::GaussianBlur(in, images[0], cv::Size(1,1), 1.0);

            std::vector<std::vector<cv::KeyPoint> > keypoints;
            cv::GoodFeaturesToTrackDetector fd(maxPt, 0.001, minDist, 3, false);
            fd.detect(images, keypoints);

            for(int i = 0; i < keypoints[0].size(); i++)
            {
                cv::Point2f p = keypoints[0][i].pt;
                out.data.push_back(p);
            }

            /// Set the needed parameters to find the refined corners
            cv::Size winSize = cv::Size( 9, 9 );
            cv::Size zeroZone = cv::Size( -1, -1 );
            cv::TermCriteria criteria = cv::TermCriteria( CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001 );

            /// Calculate the refined corner locations
            cv::cornerSubPix( images[0], out.data, winSize, zeroZone, criteria );
            return true;
        }

        void MarkerDetectorCheckerboard::SetMinDistance(int minDist){
            this->minDist = minDist;
        }

        void MarkerDetectorCheckerboard::SetMaxPoint(int maxPt){
            this->maxPt = maxPt;
        }

    } // namespace vision
} // namespace cook
