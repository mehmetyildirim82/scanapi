//
// Created by yildirim on 13.06.15.
//

#include "id_marker.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

namespace cook {
    namespace vision {

        bool IdMarker::FindSelf(const ContourArray& contours, MarkerArray2d& markers, Image* mask, Image* outImg) const {
            try {
                IdMarkerArray2d* id_markers = dynamic_cast<IdMarkerArray2d*>(&markers);
                if(!id_markers)
                    return false;

                if(!mask)
                    return false;

                std::map<int, int> id_markers_index;
                //Find the rotated rectangles and ellipses for each contour
                std::vector<cv::RotatedRect> minEllipse( contours.size() );

                for( size_t i = 0; i < contours.size(); i++ )
                {
                    if( contours[i].size() > 5 )
                    {
                        minEllipse[i] = cv::fitEllipse( cv::Mat(contours[i]) );
                    }
                }

                cv::RNG rng(12345);
                //Draw contours + rotated rects + ellipses
                for( size_t i = 0; i< minEllipse.size(); i++ )
                {
                    cv::RotatedRect& rect = minEllipse[i];
                    if(rect.size.width < minSize.width || rect.size.height < minSize.height)continue;
                    if(rect.size.width > maxSize.width || rect.size.height > maxSize.height)continue;

                    int teeth = GetTeethAmount(*mask, rect.center, rect.size.width/2, radialStepAmount, radialOffset);
                    if(!teeth)
                        continue;

                    teeth = static_cast<int>(std::ceil(teeth/2.0));

                    if(teeth>MIN_TEETH_MARKER){
                        id_markers->index.push_back(teeth);
                        id_markers->data.push_back(rect.center);
                    }
                }
                return true;
            }
            catch(...) {
                return false;
            }
        }

        int IdMarker::GetTeethAmount(const Image& mask, const Point2f& center, const int radius, const int radialStepAmount, const int radialOffset) const {
            try {
                unsigned int tooth_counter = 0;

                cv::Point2f p = cv::Point2f( radius - radialOffset );
                cv::Point2f image_coord = center + p;
                uchar prev_val = mask.at<uchar>(image_coord);

                float step_size = COOK_PI * 2.0f / (float)radialStepAmount;
                for(int j = 0; j < radialStepAmount; j++)
                {
                    cv::Point2f rotated_p;
                    rotated_p.x = (p.x*std::cos(step_size*j) - p.y*std::sin(step_size*j));
                    rotated_p.y = (p.x*std::sin(step_size*j) + p.y*std::cos(step_size*j));
                    image_coord = center + rotated_p;

                    //analyze this value to detect teeth
                    uchar curr_val = mask.at<uchar>(image_coord);
                    if(abs(curr_val - prev_val) == 255)
                        tooth_counter++;
                    prev_val = curr_val;
                }
                return tooth_counter;
            }
            catch(...) {
                return 0;
            }
        }

    } // namespace vision
} // namespace cook