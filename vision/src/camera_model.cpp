//
// Created by yildirim on 20.06.15.
//

#include "camera_model.h"
#include <opencv2/calib3d/calib3d.hpp>

#include <exceptions.h>
#include <conversion.h>
#include <iostream>

namespace cook {
    namespace vision {

        CameraModel::CameraModel() : geometricError(1000.0) {
            distortionCoeff = cv::Mat::zeros(5,1,CV_64F);
        }

        bool CameraModel::Init(ProjectionMatrix& P) {
            projection = P;

			cv::Scalar sum = cv::sum(P.Get());
			if(!(sum[0] + sum [1] + sum[2] + sum[3]))
				return false;

            try{
				Decompose();
				return true;
			}
			catch (...) {
				return false;
			}
        }

        void CameraModel::Recompose() {
            projection.Compose(intrinsics, rotation, projectionCenter);
        }

        void CameraModel::Decompose() {
            projection.Decompose(intrinsics, rotation, projectionCenter);
        }

        IntrinsicMat& CameraModel::GetIntrinsics() {
            return intrinsics;
        }

        Mat& CameraModel::GetRotation() {
            return rotation;
        }

        Mat& CameraModel::GetProjectionCenter() {
            return projectionCenter;
        }

        DistortionCoeff& CameraModel::GetDistortionCoeff() {
            return distortionCoeff;
        }

        ProjectionMatrix& CameraModel::GetProjectionMat() {
            return projection;
        }

        double& CameraModel::GetAccuracy() {
            return geometricError;
        }

        ImageSize& CameraModel::GetImageSize() {
            return size;
        }

        void CameraModel::DumpModel(std::ofstream& file) {
            file << "K:" << intrinsics.Get() << std::endl;
            file << "R:" << rotation.Get() << std::endl;
            file << "T:" << projectionCenter.Get() << std::endl;
            file << "D:" << distortionCoeff << std::endl;
        }

        bool CameraModel::ProjectPoints(const Point3dArray& p3dArray, Point2dArray& p2dArray, bool doUndistortion)const {
            try {
                if(doUndistortion) {
                    cv::Mat dist = distortionCoeff;
                    if(!dist.at<double>(0))
                        dist = cv::Mat::zeros(5,1,CV_64F);
                    cv::projectPoints(
                            p3dArray,
                            rotation.GetReadOnly(),
                            projectionCenter.GetReadOnly(),
                            intrinsics.GetReadOnly(),
                            dist,
                            p2dArray
                    );
                }
                else {
                    for(size_t i = 0; i< p3dArray.size(); i++) {
                        Point2f p2f = projection.Project(p3dArray[i]);
                        p2dArray.push_back(p2f);
                    }
                }
                return true;
            }
            catch(...) {
                throw exceptionOpenCV();
            }
        }

        float CameraModel::TestProjection(const Point3f& p3f, const Point2f& p2f, bool doUndistortion)const {
            try {

                Point3dArray in;
                in.push_back(p3f);

                Point2dArray out;
                ProjectPoints(in, out, doUndistortion);

                return sqrt((out[0].x - p2f.x)*(out[0].x - p2f.x) + (out[0].y - p2f.y)*(out[0].y - p2f.y)); //RMS
            }
            catch(...) {
                throw exceptionOpenCV();
            }
        }

    } // namespace vision
} // namespace cook
