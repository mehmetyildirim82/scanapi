//
// Created by yildirim on 08.06.15.
//

#include "contour_extractor.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace cook {
    namespace vision {

        bool ContourExtractor::Extract(const Image& input, ContourArray& contours) {
            try {
                result = input.clone();

                //binarize image
                cv::threshold( result, result, binaryThreshold, 255, CV_THRESH_BINARY);

				cv::imwrite("binary.bmp", result);

                result.copyTo(mask);

                std::vector<cv::Vec4i> hierarchy;
                cv::findContours( result, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0) );

                // iterate through all the top-level contours,
                // draw each connected component with its own random color
                int idx = 0;
                for( ; idx >= 0; idx = hierarchy[idx][0] )
                {
                    cv::Scalar color( rand()&255, rand()&255, rand()&255 );
                    cv::drawContours( result, contours, idx, color );
                }
				cv::imwrite("contours.bmp", result);

                return true;
            }
            catch(...) {
                return false;
            }
        }

        const Image& ContourExtractor::GetContourImage() const {
            return result;
        }

        const Image& ContourExtractor::GetBinaryImage() const {
            return mask;
        }
    }
}
