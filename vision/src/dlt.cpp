//
// Created by yildirim on 05.06.15.
//

#include "dlt.h"
#include <condition_matrix.h>

namespace cook {
    namespace vision {

        bool DLT(const Point3dArray& p3d, const Point2dArray& p2d, ProjectionMatrix& P) {

            if(p3d.size() != p2d.size())
                return false;

            if(p3d.size() < 6)
                return false;

            ConditionMatrix<Point3f> condition_matrix_3d(p3d);
            ConditionMatrix<Point2f> condition_matrix_2d(p2d);

            Point3dArray conditioned_3d;
            Point2dArray conditioned_2d;
            condition_matrix_3d.Condition(p3d, conditioned_3d);
            condition_matrix_2d.Condition(p2d, conditioned_2d);

            //Direct Linear Transformation
            //Prepare design(A) matrix
            cv::Mat A(12, 12, CV_32F);
            for(size_t i = 0; i < p3d.size(); i++ )
            {
                A.at<float>(cv::Point2i(0,i*2)) = -1*conditioned_3d[i].x;
                A.at<float>(cv::Point2i(1,i*2)) = -1*conditioned_3d[i].y;
                A.at<float>(cv::Point2i(2,i*2)) = -1*conditioned_3d[i].z;
                A.at<float>(cv::Point2i(3,i*2)) = -1*1;
                A.at<float>(cv::Point2i(4,i*2)) = 0.0;
                A.at<float>(cv::Point2i(5,i*2)) = 0.0;
                A.at<float>(cv::Point2i(6,i*2)) = 0.0;
                A.at<float>(cv::Point2i(7,i*2)) = 0.0;
                A.at<float>(cv::Point2i(8,i*2)) = conditioned_2d[i].x*conditioned_3d[i].x;
                A.at<float>(cv::Point2i(9,i*2)) = conditioned_2d[i].x*conditioned_3d[i].y;
                A.at<float>(cv::Point2i(10,i*2)) = conditioned_2d[i].x*conditioned_3d[i].z;
                A.at<float>(cv::Point2i(11,i*2)) = conditioned_2d[i].x*1;

                A.at<float>(cv::Point2i(0,i*2+1)) = 0.0;
                A.at<float>(cv::Point2i(1,i*2+1)) = 0.0;
                A.at<float>(cv::Point2i(2,i*2+1)) = 0.0;
                A.at<float>(cv::Point2i(3,i*2+1)) = 0.0;
                A.at<float>(cv::Point2i(4,i*2+1)) = -1*conditioned_3d[i].x;
                A.at<float>(cv::Point2i(5,i*2+1)) = -1*conditioned_3d[i].y;
                A.at<float>(cv::Point2i(6,i*2+1)) = -1*conditioned_3d[i].z;
                A.at<float>(cv::Point2i(7,i*2+1)) = -1*1;
                A.at<float>(cv::Point2i(8,i*2+1)) = conditioned_2d[i].y*conditioned_3d[i].x;
                A.at<float>(cv::Point2i(9,i*2+1)) = conditioned_2d[i].y*conditioned_3d[i].y;
                A.at<float>(cv::Point2i(10,i*2+1)) = conditioned_2d[i].y*conditioned_3d[i].z;
                A.at<float>(cv::Point2i(11,i*2+1)) = conditioned_2d[i].y*1;
            }

            //prepare parametric space
            cv::Mat p(12,1,CV_32F);

            //Solve for 12 parameters
            cv::SVD::solveZ(A, p);

            //TODO: Handle re-shaping better
            //Reshape function
            Mat temp(3,4);
            temp.At(0,0) = p.at<float>(0);
            temp.At(0,1) = p.at<float>(1);
            temp.At(0,2) = p.at<float>(2);
            temp.At(0,3) = p.at<float>(3);
            temp.At(1,0) = p.at<float>(4);
            temp.At(1,1) = p.at<float>(5);
            temp.At(1,2) = p.at<float>(6);
            temp.At(1,3) = p.at<float>(7);
            temp.At(2,0) = p.at<float>(8);
            temp.At(2,1) = p.at<float>(9);
            temp.At(2,2) = p.at<float>(10);
            temp.At(2,3) = p.at<float>(11);

            //Reverse condition
            temp = condition_matrix_2d.Get().inv()*temp.Get()*condition_matrix_3d.Get();
            NormaliseHomogenous(temp);

            temp.Get().convertTo(temp.Get(), CV_64F);
            P = ProjectionMatrix(temp);
            return true;
        }

    } // namespace vision
} // namespace cook
