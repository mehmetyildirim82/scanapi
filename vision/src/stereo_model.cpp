//
// Created by yildirim on 21.06.15.
//

#include "stereo_model.h"

namespace cook {
    namespace vision {

        void StereoModel::SetCamera(CameraModel& camera, CameraIndex idx) {
            cam[static_cast<int>(idx)] = camera;
        }

        CameraModel& StereoModel::GetCamera(CameraIndex idx) {
            return cam[static_cast<int>(idx)];
        }

        Mat& StereoModel::GetRotation() {
            return rotation;
        }

        Mat& StereoModel::GetTranslation() {
            return translation;
        }

        Mat& StereoModel::GetEssentialMatrix() {
            return essentialMatrix;
        }

        Mat& StereoModel::GetFundamentalMatrix() {
            return fundamentalMatrix;
        }

        void StereoModel::DumpModel(std::ofstream& file) {
            file << "R:" << rotation.Get() << std::endl;
            file << "T:" << translation.Get() << std::endl;
            file << "E:" << essentialMatrix.Get() << std::endl;
            file << "F:" << fundamentalMatrix.Get() << std::endl;
        }

    } // namespace vision
} // namespace cook