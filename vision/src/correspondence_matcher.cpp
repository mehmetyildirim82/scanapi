//
// Created by yildirim on 14.06.15.
//

#include "correspondence_matcher.h"
#include "marker_data.h"
#include <algorithm>
#include <opencv2/core/core.hpp>
#include <opencv2/flann/miniflann.hpp>
#include <interpolation.h>
#include <iostream>
#include <reconstruction.h>

namespace cook {
    namespace vision {

        bool CorrespondenceMatcher::Match(const IdMarkerArray3d& idMarkerWorld,
                                          const IdMarkerArray2d& idMarkerImage,
                                          Correspondence3d& correspondence) const {
            try{
                if(idMarkerWorld.data.empty() || idMarkerImage.data.empty()){
                    return false;
                }

                if(idMarkerWorld.data.size() != idMarkerWorld.index.size()){
                    return false;
                }

                if(idMarkerImage.data.size() != idMarkerImage.index.size()){
                    return false;
                }

                for(size_t i = 0; i < idMarkerWorld.index.size(); i++) {
                    int indexWrld = idMarkerWorld.index[i];
                    for(size_t j = 0; j < idMarkerImage.index.size(); j++) {
                        int indexImg = idMarkerImage.index[j];
                        if(indexWrld == indexImg) {
                            correspondence.from.push_back(idMarkerWorld.data[i]);
                            correspondence.to.push_back(idMarkerImage.data[j]);
							correspondence.index.push_back(indexWrld);
                        }
                    }
                }

                return true;
            }
            catch (...){
                return false;
            }
        }

        bool CorrespondenceMatcher::Match( const MarkerArray3d& MarkerWorld,
                                           const MarkerArray2d& MarkerImage,
                                           Correspondence3d& correspondence,
                                           const ProjectionMatrix& P,
                                           Image* debug_img,
                                           float search_radius) const
        {
            try {
                cv::flann::KDTreeIndexParams indexParams;
                cv::flann::Index kdtree(cv::Mat(MarkerImage.data).reshape(1), indexParams);

                for(size_t i = 0; i < MarkerWorld.data.size(); i++) {
                    Point2f projected = P.Project(MarkerWorld.data[i]);

                    if(debug_img) {
                        cv::circle( *debug_img, projected, 3,cv::Scalar(0,0,0));
                        std::ostringstream oss;
                        oss << i;
                        cv::putText(*debug_img, oss.str(), cv::Point2f(projected.x - 25, projected.y), cv::FONT_HERSHEY_SIMPLEX, .5, cv::Scalar(0, 255, 255));
                    }

                    std::vector<float> query;
                    query.push_back(projected.x); //Insert the 2D point we need to find neighbours to the query
                    query.push_back(projected.y); //Insert the 2D point we need to find neighbours to the query
                    std::vector<int> indices;
                    std::vector<float> dists;
                    bool result = (bool)kdtree.radiusSearch(query, indices, dists, search_radius, 1);

                    if(!result)
                        continue;

                    // item found
                    correspondence.index.push_back(i);
                    correspondence.from.push_back(MarkerWorld.data[i]);
                    correspondence.to.push_back(MarkerImage.data[indices[0]]);

                    if(debug_img) {
                        cv::circle( *debug_img, correspondence.to[correspondence.to.size()-1], 3,cv::Scalar(255,255,255));
                        std::ostringstream oss;
                        oss << i;
                        cv::putText(*debug_img, oss.str(), cv::Point2f(correspondence.to[correspondence.to.size()-1].x - 25, correspondence.to[correspondence.to.size()-1].y), cv::FONT_HERSHEY_SIMPLEX, .5, cv::Scalar(255, 255, 255));
                    }
                }
                return true;
            }
            catch(...) {
                return false;
            }
        }

        bool CorrespondenceMatcher::Match(const Correspondence3d& cam01,
                   const Correspondence3d& cam02,
                   StereoCorrespondence& stereo) const
        {
            if(cam01.index.size() != cam01.from.size())
                return false;

            if(cam02.index.size() != cam02.from.size())
                return false;

            for(size_t i = 0; i < cam01.from.size(); i++) {
                unsigned int index = cam01.index[i];
                for(size_t j = 0; j < cam02.from.size(); j++) {
                    if(index == cam02.index[j]) {
                        stereo.index.push_back(index);
                        stereo.world.push_back(cam01.from[i]);
                        stereo.image01.push_back(cam01.to[i]);
                        stereo.image02.push_back(cam02.to[j]);
                    }
                }
            }
            return true;
        }


        bool CorrespondenceMatcher::Match(  const CorrespondenceAbsolute& cam01,
                                            const CorrespondenceAbsolute& cam02,
                                            StereoCorrespondence& stereo) const
        {
            if(cam01.absoluteValue.size() != cam01.geometric.from.size())
                return false;

            if(cam02.absoluteValue.size() != cam02.geometric.to.size())
                return false;

            for(size_t i = 0; i < cam01.geometric.from.size(); i++) {
                Point3f p1 = cam01.geometric.from[i];
                for(size_t j = 0; j < cam02.geometric.from.size(); j++) {
                    if(p1 == cam02.geometric.from[j]) {
                        stereo.world.push_back(cam01.geometric.from[i]);
                        stereo.image01.push_back(cam01.geometric.to[i]);
                        stereo.image02.push_back(cam02.geometric.to[j]);
                    }
                }
            }
            return true;
        }

        bool CorrespondenceMatcher::Match( StereoModel& model,
                                           ProjectorModel& projector,
                                           const Image& img01,
                                           const Image& img02,
                                           const EpipolarBundle& epipolar,
                                           Correspondence2d& corr) const {
            try {
                int counter = 0;
                Point2dArray source;
                Point2dArray::iterator ptL = corr.from.begin();
                EpipolarBundle::const_iterator epi = epipolar.begin();
                CameraModel& cam01 = model.GetCamera(StereoModel::Camera1);
                CameraModel& cam02 = model.GetCamera(StereoModel::Camera2);

                for(;ptL != corr.from.end();++ptL, ++epi) {
                    //absolute phase value at the source image
                    double srcW = BilinearInterpolation<short>(*ptL, img01);
                    if(!srcW)continue;

                    //calculate 2d line from epipolar line equation
                    Line2d l;
                    ConvertLinearEqTo2DLine(*epi, 0, 2000, l);

                    //Estimate 3d point from single view
                    Point3f estPt3;
                    if(!Estimate3DPoint(ptL->x, ptL->y, srcW, cam01, projector, estPt3))
                        continue;

                    Point2f estPt2 = cam02.GetProjectionMat().Project(estPt3);

                    if(!ReduceEpipolarSearchSpace(estPt2, 20.0, l))
                        continue;

                    Point2f pt2d_corr;
                    if(!InterpolateValueOnLine<short>(img02, l, srcW, pt2d_corr))
                        continue;

                    source.push_back(*ptL);
                    corr.to.push_back(pt2d_corr);
                }
                corr.from.swap(source);
                return true;
            }
            catch (...) {
                return false;
            }
        }

    } // namespace vision
} // namespace cook
