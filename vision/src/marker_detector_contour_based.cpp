//
// Created by yildirim on 08.06.15.
//

#include "marker_detector_contour_based.h"

namespace cook {
    namespace vision {

        bool MarkerDetectorContourBased::Detect(const Image& in, MarkerArray2d& out, Image* outImg) {
            try {
                if(!marker)
                    return false;

                if(!contourExtractor)
                    return false;

                ContourArray contourArray;
                contourExtractor->Extract(in, contourArray);

                if(outImg)
                    in.copyTo(*outImg);

                Image mask;
                contourExtractor->GetBinaryImage().copyTo(mask);
	
                bool result = marker->FindSelf(contourArray, out, &mask, outImg);
                return result;
            }
            catch(...) {
                return false;
            }
        }

        void MarkerDetectorContourBased::SetMarker(Marker::Ptr marker) {
            this->marker = marker;
        }

        void MarkerDetectorContourBased::SetContourExtractor(ContourExtractor::Ptr extractor) {
            this->contourExtractor = extractor;
        }

        const ContourExtractor::Ptr MarkerDetectorContourBased::GetContourExtractor() const {
            return contourExtractor;
        }

    } // namespace vision
} // namespace cook