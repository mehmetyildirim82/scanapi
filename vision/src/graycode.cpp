//
// Created by yildirim on 27.06.15.
//

#include "graycode.h"
#include <opencv2/highgui/highgui.hpp>
#include <boost/math/special_functions/round.hpp>
#include <cmath>
#include <core_definitions.h>
#include <utilities.h>

#include <iostream>

using namespace std;

namespace cook {
    namespace vision {

        bool CreateGrayCodePattern(Image& img, short layer, bool invers /* = false*/, cv::Scalar color) {

            //layer can't be bigger than 8 and less than 0
            if( layer < 0 || layer > 8 )
                return false;

            unsigned char *pattern, *line;
            unsigned long i;

            int width = 1024;
            int height = 768;

            img = cv::Mat::zeros(height, width, CV_8UC3);
            std::vector<Image> channels;
            cv::split(img, channels);

            pattern = new unsigned char[width];
            layer = short(10 - layer);

            //Pattern
            for( i=0; i < width; i++ )
            {
                if( IsBitSet( GrayCode( i ), layer ) )
                if( invers ) pattern[i] = 0;
                else	 pattern[i] = 255;
                else
                if( invers ) pattern[i] = 255;
                else	 pattern[i] = 0;
            }

            for( i=0; i< height; i++ ) {
                for(int j = 0; j < 3; j++){
                    if(color[j]){
                        line = (unsigned char*)channels[j].row(i).data;
                        memcpy( line, pattern, width );
                    }
                }
            }

            cv::merge(channels, img);

            delete []pattern;
            return true;
        }

        bool IsBitSet( unsigned long code, short bit )
        {
            const unsigned long mask = (unsigned long) pow(2.f, bit);
            return (mask & code) != 0;
        }

        unsigned long GrayCode(unsigned long n)
        {
            return n ^ ( n >> 1 );
        }

        bool CreatePhasePattern(Image& img, short layer, cv::Scalar color)
        {
            unsigned char *pattern, *line;
            unsigned long i;

            int width = 1024;
            int height = 768;

            img = cv::Mat::zeros(height, width, CV_8UC3);
            std::vector<Image> channels;
            cv::split(img, channels);

            const int image_offset = (static_cast<int>(width) - static_cast<int>(width)) / 2;

            pattern = new unsigned char[width];
            layer = (short)(3 - layer);

            const unsigned int period = 2 * static_cast<int>( pow(2.f, static_cast<int>(10-(9-1))) );

            unsigned int k = layer*2;

            for( i=0; i < width; i++) {
                pattern[i] = (unsigned char) (127 * (1 + sin(2 * COOK_PI * ((i)%period + k + 0.5) / period)));
                //if( IsBitSet( Phase( i ), layer ) ) pattern[i] = (unsigned char) (127 * (1 + sin(2 * M_PI * ((i)%period + k + 0.5) / period)));
                //else pattern[i] = 0;
            }

            for( i=0; i< height; i++ ) {
                for(int j = 0; j < 3; j++){
                    if(color[j]){
                        line = (unsigned char*)channels[j].row(i).data;
                        memcpy( line, pattern, width );
                    }
                }
            }

            cv::merge(channels, img);

            delete []   pattern;

            return true;
        }

        unsigned long Phase(unsigned long n)
        {
            short i;
            unsigned int res = 0;
            char q = (char) (n % 4);
            double s;

            for ( i = 0; i < 4; i++)
            {
                s = sin( (double)(i*2.0+1.0) / 4.0 * COOK_PI - (double)q * COOK_PI );
                if( s > 0 )
                    res = res + ( 1 << i );
            }

            return res;
        }

        bool CalculateGrayCodeImage(const ImageSequence& sequence, Image& graycode) {

            // Check if we have correct amount of images
            if(sequence.empty() || sequence.size() != 16)
            {
                return false;
            }

            // Scan images must be in grayscale
            for( int i=0; i<sequence.size(); i++ )
            {
                if( sequence[i].type() != CV_8UC1 )
                {
                    return false;
                }
            }

            int width = sequence[0].cols;
            int height = sequence[0].rows;

            graycode = Image(height, width, CV_8UC1);

            unsigned char *normal = NULL;
            unsigned char *invers = NULL;
            unsigned char *result = NULL;
            int          h, BitPlane;
            unsigned int k;

            try {
                for( unsigned int i=0; i < height; i++ )
                {
                    result = (unsigned char*)graycode.row(i).data;
                    memset(result, 0, graycode.cols );

                    for( long j=0; j<=7; j++ )
                    {
                        normal = (unsigned char*)sequence[2*j].row(i).data;
                        invers = (unsigned char*)sequence[2*j+1].row(i).data;
                        BitPlane = 1 << (7-j);

                        for( k=0; k < width; k++ )
                        {
                            if (normal[k] > invers[k]){
                                result[k] = (unsigned char)( result[k] | BitPlane );
                            }
                        }
                    }

                    for( k=0; k < width; k++ )
                    {
                        h = result[k];
                        // inverse graycode calculation
                        result[k] = (unsigned char)( LinearCode( h & 255 ) );
                    }
                }
            }
            catch(...)
            {
                return false;
            }

            return true;
        }

        bool CalculatePhaseImage(const ImageSequence& sequence, Image& absolutePhase)
        {
            if(sequence.empty() || sequence.size() != 4)
            {
                return false;
            }

            int width = sequence[0].cols;
            int height = sequence[0].rows;

            absolutePhase = Image(height, width, CV_8UC1);

            unsigned char *P;
            unsigned char *Q;
            unsigned char *S;
            unsigned char *T;
            unsigned char *result;

			double Scale = 128.0/CV_PI;
            long A,B;

            try{
                // all rows
                for( long i=0; i<height; i++ )
                {
                    P = sequence[0].row( i ).data;
                    Q = sequence[1].row( i ).data;
                    S = sequence[2].row( i ).data;
                    T = sequence[3].row( i ).data;
                    result = (unsigned char*)absolutePhase.row( i ).data;
                    // all columns
                    for( long j=0; j<width; j++ ) {
                        A = S[j] - P[j];
                        B = T[j] - Q[j];
                        if ((A == 0) && (B == 0)) result[j] = (unsigned char)(0);
                        else result[j] = (unsigned char)boost::math::lround(atan2((double)A, (double)B)*Scale);
                    }
                }
            }
            catch(...) {
                return false;
            }
            return true;
        }

        bool GenerateMaskFromPhase(const ImageSequence& phaseIn, int DynamicThreshold, int MaximumThreshold, int SinusThreshold, Image& maskOut) {
            if(phaseIn.size() != 4)
                return false;

            const Image& Phase1 = phaseIn[0];
            const Image& Phase2 = phaseIn[1];
            const Image& Phase3 = phaseIn[2];
            const Image& Phase4 = phaseIn[3];

            long width = (long)(Phase1.cols);
            long height = (long)(Phase1.rows);

            maskOut = Image::zeros(height, width, CV_8UC1);

            unsigned char* P;
            unsigned char* Q;
            unsigned char* S;
            unsigned char* T;
            unsigned char* R;
            long Min,Max;

            try{
                for (long I = 0; I < height; I++)
                {
                    P = Phase1.row(I).data;
                    Q = Phase2.row(I).data;
                    S = Phase3.row(I).data;
                    T = Phase4.row(I).data;
                    R = maskOut.row(I).data;

                    for (long J = 0; J < width; J++)
                    {
                        Min = (*P);
                        Max = (*P);
                        if ((*Q) < Min) Min = (*Q); else if ((*Q) > Max) Max = (*Q);
                        if ((*S) < Min) Min = (*S); else if ((*S) > Max) Max = (*S);
                        if ((*T) < Min) Min = (*T); else if ((*T) > Max) Max = (*T);

                        if( (Max-Min)>=DynamicThreshold )
                        {
                            if( MaximumThreshold==255 ) {
                                if( (*P) <MaximumThreshold && (*Q)<MaximumThreshold && (*S)<MaximumThreshold && (*T)<MaximumThreshold )
                                    *R = (unsigned char)(255);
                                else if( abs( ( (*P) + (*S) ) - ( (*Q) + (*T) ) ) <= SinusThreshold )
                                    *R = (unsigned char)(255);
                                else
                                    *R = (unsigned char)(0);
                            }
                            else {
                                if( (*P)<=MaximumThreshold && (*Q)<=MaximumThreshold && (*S)<=MaximumThreshold && (*T)<=MaximumThreshold )
                                    *R = (unsigned char)(255);
                                else
                                    *R = (unsigned char)(0);
                            }
                        }
                        else *R = (unsigned char)(0);

                        P++; Q++; S++; T++; R++;
                    }
                }
            }
            catch (...) {
                return false;
            }
            return true;
        }

        bool GenerateAbsolutePhase(const Image& phase, const Image& graycode, Image& absPhase) {

            if(phase.empty() || graycode.empty())
                return false;

            unsigned char* GC;
            unsigned char* Ph;
            unsigned short* APh;
            int c,p,PRI;

            int width = graycode.cols;
            int height = graycode.rows;

            absPhase = Image::zeros( height, width, CV_16U );

            PRI = 64;

            try {
                for(long j=0; j < height  ; j++)
                {
                    GC = graycode.row(j).data;
                    Ph = phase.row(j).data;
                    APh = (unsigned short*)absPhase.row(j).data;

                    for (long I = 0; I < width; I++)
                    {
                        c = (*GC);
                        p = (char)(*Ph);
                        if ((c < 1)||(c > 254))
                        {
                            *APh = 0;
                        }
                        else
                        {
                            if (p < -PRI)
                            {
                                if (c & 1)
                                {
                                    c++;
                                }
                            }
                            else
                            {
                                if (p > PRI)
                                {
                                    if (!(c & 1))
                                    {
                                        c--;
                                    }
                                }
                            }
                            *APh = (unsigned short)(((c >> 1) << 8) + p);
                        }
                        GC++; Ph++; APh++;
                    }
                }
            }
            catch (...) {
                return false;
            }
            return true;
        }

        bool LoadAbsolutePhase(const char* filename, Image& absPhase) {
            if(!CheckFile(filename))
                return false;

            //TODO: Check if file is a 16-bit tiff file

            absPhase = cv::imread(filename, CV_16U);

            short result = 0;
            try {
                for(int r = 0; r < absPhase.rows; r++)
                {
                    for(int c = 0; c < absPhase.cols; c++)
                    {
                        unsigned short val = absPhase.at<unsigned short>(cv::Point2i(c,r));

                        result = val;
                        if(val > 32767)
                            result = val - 65536;

                        absPhase.at<short>(cv::Point2i(c,r)) = result;
                    }
                }
            }
            catch (...) {
                return false;
            }

            return true;
        }

        unsigned long LinearCode(unsigned long n)
        {
            unsigned long idiv;
            int ish = 1;
            unsigned long ans = n;

            for(;;)
            {
                ans ^= ( idiv = ans >> ish );
                if( idiv <= 1 || ish == 16 )
                    return ans;
                ish <<= 1;
            }
        }

		bool CreateAbsolutePhaseImageFromScanData(ScanDataGrayCode::Ptr scanData, Image& absPhaseImage, int mask_parameter)
		{
			//calculate absolute phase image
			Image gray;
			if(!CalculateGrayCodeImage(scanData->GetDataSection(ScanDataGrayCode::GRAYCODE), gray))
			{
				throw std::exception("Can't calculate a graycode image");
			}

			Image phase;
			if(!CalculatePhaseImage(scanData->GetDataSection(ScanDataGrayCode::PHASESHIFT), phase))
			{
				throw std::exception("Can't calculate a phase image");
			}

			Image mask;
			if(!GenerateMaskFromPhase(scanData->GetDataSection(ScanDataGrayCode::PHASESHIFT), mask_parameter, 255, 5, mask))
			{
				throw std::exception("Can't calculate mask for phase image");
			}

			Image phase_masked;
			phase.copyTo(phase_masked, mask);

			Image graycode_masked;
			gray.copyTo(graycode_masked, mask);

			if(!GenerateAbsolutePhase(phase_masked, graycode_masked, absPhaseImage))
			{
				std::exception("Can't calculate an absolute phase image");
			}
			return true;
		}

#if 0
void CGrayCode::CreateGraycode(unsigned char *gc, unsigned int nPixsPerRow)const
{
	unsigned int g;
    const unsigned int step = 2*nPixsPerRow;
    unsigned char* lgc = gc;
    const int image_offset = (static_cast<int>(m_nPixsPerRow) - static_cast<int>(nPixsPerRow)) / 2;	// to center a cropped area

	for( unsigned int patternIdx=0; patternIdx<m_nPatterns; patternIdx++ )
	{
		for( unsigned int j=0; j<nPixsPerRow; j++ )
		{
			if( static_cast<int>(j) < -image_offset || j>=m_nPixsPerRow-image_offset ) {	// wenn das zu f�llende Bild gr��er als die Mustervorlage ist -> Randbereich bleibt schwarz
				lgc[j] = 0;
				lgc[nPixsPerRow + j] = 0;
				continue;
			}

			g = CalcGrayCode( image_offset + j, m_nPatternPlanes-patternIdx );

			if(g) {
                lgc[j] = 255;
				lgc[nPixsPerRow + j] = 0;
			}
			else {
				lgc[j] = 0;
				lgc[nPixsPerRow + j] = 255;
			}
		}
        lgc += step;
	}

    CreatePhaseShift(gc, nPixsPerRow);
}

void CGrayCode::CreatePhaseShift(unsigned char *gc, unsigned int nPixsPerRow)const
{
	const unsigned int period = 2 * static_cast<int>( pow(2.f, static_cast<int>(m_nPatternPlanes-(m_nPatterns-1))) );
	const int image_offset = (static_cast<int>(m_nPixsPerRow) - static_cast<int>(nPixsPerRow)) / 2;	// to center a cropped area

	for( unsigned int i=2*m_nPatterns, k=0; i<2*m_nPatterns+m_nPhaseImages; i++,k+=2 )
		for( unsigned int j=0; j<nPixsPerRow; j++ ) {
			if( static_cast<int>(j) < -image_offset || j>=m_nPixsPerRow-image_offset )	// wenn das zu f�llende Bild gr��er als die Mustervorlage ist -> Randbereich bleibt schwarz
				gc[i*nPixsPerRow + j] = 0;
			else
				gc[i*nPixsPerRow + j] = (unsigned char) (127 * (1 + sin(2 * M_PI * ((image_offset+j)%period + k + 0.5) / period)));
		}
}

// 0 - schwarz, 1 - weiss
inline unsigned int CGrayCode::CalcGrayCode(unsigned int pix, unsigned int plane)const
{
	return ((pix ^ (pix >> 1)) >> plane) & 1;
}

#endif 

    } // namespace vision
} // namespace cook
