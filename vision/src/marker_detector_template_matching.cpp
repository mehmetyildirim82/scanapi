#include "marker_detector_template_matching.h"
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <ring_marker.h>
#include <contour_extractor.h>
#include <marker_detector_contour_based.h>

namespace cook {
	namespace vision {

		bool MarkerDetectorTemplateMatching::Detect(const Image& in, MarkerArray2d& out, Image* outImg)
		{
			IdMarkerArray2d* id_markers = dynamic_cast<IdMarkerArray2d*>(&out);
			if(!id_markers)
				return false;

			if(in.empty())
				return false;

			if(templates.empty())
				return false;

#if 0
			cook::vision::Marker::Ptr ring_marker(new cook::vision::RingMarker);
			ring_marker->GetMinSize() = cook::Point2f(10, 10);
			ring_marker->GetMaxSize() = cook::Point2f(60, 60);
			cook::vision::ContourExtractor::Ptr ce(new cook::vision::ContourExtractor);
			cook::vision::MarkerDetectorContourBased mdcb;
			mdcb.SetMarker(ring_marker);
			mdcb.SetContourExtractor(ce);
#endif

			for(int i = 0; i < templates.size(); i++)
			{
				cv::Mat result;
				cv::Mat& templ = templates[i];

				/// Create the result matrix
				int result_cols =  in.cols - templ.cols + 1;
				int result_rows = in.rows - templ.rows + 1;

				result.create( result_rows, result_cols, CV_32FC1 );

				/// Do the Matching and Normalize
				cv::matchTemplate( in, templ, result, 4 );
				normalize( result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );

				/// Localizing the best match with minMaxLoc
				double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
				cv::Point matchLoc;
				cv::minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );
				matchLoc = maxLoc;
#if 0
				cv::Mat detection_region = in(cv::Rect(matchLoc.x, matchLoc.y, templ.cols, templ.rows));
				cook::vision::MarkerArray2d center;
				//find center of the circle within
				mdcb.Detect( detection_region, center);
				center.data[0].x += matchLoc.x;
				center.data[0].y += matchLoc.y;
#endif
				
				cv::Point center;
				center.x = matchLoc.x + templ.cols / 2.0;
				center.y = matchLoc.y + templ.rows / 2.0;

				id_markers->index.push_back(i+1);
				id_markers->data.push_back(center);
			}
			return true;
		}

		void MarkerDetectorTemplateMatching::SetTemplates(std::vector<cook::Image> templates)
		{
			this->templates = templates;
		}

	}
}