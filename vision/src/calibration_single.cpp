//
// Created by yildirim on 20.06.15.
//

#include "calibration_single.h"
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <exceptions.h>
#include <numerical.h>
#include <iostream>

namespace cook {
    namespace vision {

        double CalibrationSingle::Calibrate(const Correspondence3d& correspondence, CameraModel& model) {
            try {
                double& RMS = model.GetAccuracy();

                std::vector<cv::Mat> rotation(1);
                rotation[0] = cv::Mat::zeros(3,1, CV_64F);

                std::vector<cv::Mat> translation(1);
                translation[0] = cv::Mat::zeros(3,1, CV_64F);

                std::vector<Point3dArray> from(1);
                std::vector<Point2dArray> to(1);
                from[0] = correspondence.from;
                to[0] = correspondence.to;
                model.GetIntrinsics().At(0,1) = 0;
                model.GetIntrinsics().At(1,0) = 0;

                RMS = cv::calibrateCamera(from,
                                          to,
                                          model.GetImageSize(),
                                          model.GetIntrinsics().Get(),
                                          model.GetDistortionCoeff(),
                                          rotation,
                                          translation,
                                          CV_CALIB_USE_INTRINSIC_GUESS);

                model.GetRotation() = rotation[0];
                model.GetProjectionCenter() = translation[0];
                model.Recompose();
                return RMS;
            }
            catch(...) {
                throw exceptionOpenCV();
            }
        }

        //TODO: Is this function exception free??
        //TODO: Use smart pointer
        double CalibrationSingle::Calibrate(const CorrespondenceAbsolute& correspondence, CameraModel& camera, ProjectorModel& projector) {
            Point3dArray pt3d = correspondence.geometric.from;
            Point2dArray pt2d = correspondence.geometric.to;
            std::vector<double> absolute = correspondence.absoluteValue;
            int amountPt = pt3d.size();

            if(!amountPt)
                throw exceptionMatrixSize();

            if(pt2d.size() != amountPt)
                throw exceptionMatrixSize();

            double** Mat;

            int rowCount = amountPt*2;
            int colCount = 11;

            double T, U, V, W, X, Y, Z;

            Mat = new double*[rowCount];
            for(int r = 0; r < rowCount; r++)
            {
                Mat[r] = new double[colCount+1];
            }

            for( long i = 0; i < amountPt; i++ )
            {
                long j = 2*i;
                X = pt3d[i].x;
                Y = pt3d[i].y;
                Z = pt3d[i].z;
                U = pt2d[i].x;
                V = pt2d[i].y;

                //std::cout << "X:" << X << "\tY:"<< Y << "\tZ:" << Z << "\tU:" << U << "\tV:" << V << std::endl;

                Mat[j][ 0] = X;			// dU/dA11 == dU/dL1
                Mat[j][ 1] = Y;			// dU/dA12 == dU/dL2
                Mat[j][ 2] = Z;			// dU/dA13 == dU/dL3
                Mat[j][ 3] = 1.0;		// dU/dA14 == dU/dL4
                Mat[j][ 4] = 0.0;		// dU/dA21 == dU/dL5
                Mat[j][ 5] = 0.0;		// dU/dA22 == dU/dL6
                Mat[j][ 6] = 0.0;		// dU/dA23 == dU/dL7
                Mat[j][ 7] = 0.0;		// dU/dA24 == dU/dL8
                Mat[j][ 8] = -U*X;		// dU/dA41 == dU/dL9
                Mat[j][ 9] = -U*Y;		// dU/dA42 == dU/dL10
                Mat[j][10] = -U*Z;		// dU/dA43 == dU/dL11
                Mat[j][11] = U;			// dU/dA44 == dU/dL12 == 1

                Mat[j+1][ 0] = 0.0;		// dU/dA11 == dU/dL1
                Mat[j+1][ 1] = 0.0;		// dU/dA12 == dU/dL2
                Mat[j+1][ 2] = 0.0;		// dU/dA13 == dU/dL3
                Mat[j+1][ 3] = 0.0;		// dU/dA14 == dU/dL4
                Mat[j+1][ 4] = X;		// dU/dA21 == dU/dL5
                Mat[j+1][ 5] = Y;		// dU/dA22 == dU/dL6
                Mat[j+1][ 6] = Z;		// dU/dA23 == dU/dL7
                Mat[j+1][ 7] = 1.0;		// dU/dA24 == dU/dL8
                Mat[j+1][ 8] = -V*X;	// dU/dA41 == dU/dL9
                Mat[j+1][ 9] = -V*Y;	// dU/dA42 == dU/dL10
                Mat[j+1][10] = -V*Z;	// dU/dA43 == dU/dL11
                Mat[j+1][11] = V;		// dU/dA44 == dU/dL12 == 1
            }

            if(!Householder(Mat, rowCount, colCount))
                throw exceptionNumerical("Can't solve linear equation");

            // Saving coefficients as projection matrix
            camera.GetProjectionMat().At(0,0) = static_cast<float>(Mat[ 0][colCount]);
            camera.GetProjectionMat().At(0,1) = static_cast<float>(Mat[ 1][colCount]);
            camera.GetProjectionMat().At(0,2) = static_cast<float>(Mat[ 2][colCount]);
            camera.GetProjectionMat().At(0,3) = static_cast<float>(Mat[ 3][colCount]);
            camera.GetProjectionMat().At(1,0) = static_cast<float>(Mat[ 4][colCount]);
            camera.GetProjectionMat().At(1,1) = static_cast<float>(Mat[ 5][colCount]);
            camera.GetProjectionMat().At(1,2) = static_cast<float>(Mat[ 6][colCount]);
            camera.GetProjectionMat().At(1,3) = static_cast<float>(Mat[ 7][colCount]);
            camera.GetProjectionMat().At(2,0) = static_cast<float>(Mat[ 8][colCount]);
            camera.GetProjectionMat().At(2,1) = static_cast<float>(Mat[ 9][colCount]);
            camera.GetProjectionMat().At(2,2) = static_cast<float>(Mat[10][colCount]);
            camera.GetProjectionMat().At(2,3) = 1.0;

            rowCount = 0;
            for( long i = 0; i < amountPt; i++ )
            {
                X = pt3d[i].x;
                Y = pt3d[i].y;
                Z = pt3d[i].z;
                W = absolute[i];

                if( W <= 0)
                    continue;

                Mat[rowCount][0] = X;
                Mat[rowCount][1] = Y;
                Mat[rowCount][2] = Z;
                Mat[rowCount][3] = 1.0;
                Mat[rowCount][4] = -W*X;
                Mat[rowCount][5] = -W*Y;
                Mat[rowCount][6] = -W*Z;
                Mat[rowCount][7] = W;
                rowCount++;
            }

            colCount = 7;
            if(!Householder(Mat, rowCount, colCount))
                throw exceptionNumerical("Can't solve linear equation");

            projector.GetModel().At(0,0) = static_cast<float>(Mat[0][colCount]);
            projector.GetModel().At(0,1) = static_cast<float>(Mat[1][colCount]);
            projector.GetModel().At(0,2) = static_cast<float>(Mat[2][colCount]);
            projector.GetModel().At(0,3) = static_cast<float>(Mat[3][colCount]);
            projector.GetModel().At(0,4) = static_cast<float>(Mat[4][colCount]);
            projector.GetModel().At(0,5) = static_cast<float>(Mat[5][colCount]);
            projector.GetModel().At(0,6) = static_cast<float>(Mat[6][colCount]);
            projector.GetModel().At(0,7) = 1.0;

            //Data copied already, must clean heap
            for (int i=0; i<rowCount; i++)
                delete [] Mat[i];
            delete [] Mat;

            camera.Decompose();

            return 1.0;
        }

    } // namespace vision
} // namespace cook