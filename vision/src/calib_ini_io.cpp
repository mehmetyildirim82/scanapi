//
// Created by yildirim on 19.06.15.
//

#include "calib_ini_io.h"
#include <boost/property_tree/ini_parser.hpp>
#include <boost/tokenizer.hpp>
#include <utilities.h>
#include <interpolation.h>

namespace cook {
    namespace vision {

        bool CalibIniIO::SetFile(const char* filename) {
            if(!Validate(filename))
                return false;
            return true;
        }

        bool CalibIniIO::Read(CorrespondenceAbsolute& correspondence)const {
            if(!fileValid)
                return false;

            try {
                int amount_points = pt.get<int>("RawCalibration.NP");

                if(!amount_points)
                    return false;

                //tokenize each point entry
                char buffer[32];
                typedef boost::tokenizer <boost::char_separator<char> > tokenizer;

                //set seperator type
                boost::char_separator <char> sep(" ");

                for (int i = 0; i < amount_points; i++) {
                    sprintf(buffer, "RawCalibration.P%.3i", i);
                    std::string line;
                    try{
                         line = pt.get<std::string>(buffer);
                    }
                    catch (...){
                        continue;
                    }

                    tokenizer tokens(line, sep);

                    tokenizer::iterator tok_iter = tokens.begin();
                    Point3f marker3d;
                    marker3d.x = ::atof(std::string(*tok_iter++).c_str());
                    marker3d.y = ::atof(std::string(*tok_iter++).c_str());
                    marker3d.z = ::atof(std::string(*tok_iter++).c_str());

                    Point2f marker2d;
                    marker2d.x = ::atof(std::string(*tok_iter++).c_str());
                    marker2d.y = ::atof(std::string(*tok_iter++).c_str());

                    double absolute_value = ::atof(std::string(*tok_iter).c_str());
                    correspondence.geometric.from.push_back(marker3d);
                    correspondence.geometric.to.push_back(marker2d);
                    correspondence.absoluteValue.push_back(absolute_value);
                }
                return true;
            }
            catch(...) {
                return false;
            }
        }

        bool CalibIniIO::Read(CalibrationRig& rig)const {
            if(!fileValid)
                return false;

            try {
                rig.countMarker = pt.get<int>("CalibInfo.MarkCount");

                if(!rig.countMarker)
                    return false;

                rig.countPlane = pt.get<int>("CalibInfo.PlaneCount");

                //tokenize each point entry
                char buffer[32];
                typedef boost::tokenizer <boost::char_separator<char>> tokenizer;

                //set seperator type
                boost::char_separator <char> sep(" ");

                for (size_t i = 0; i < rig.countMarker; i++) {
                    sprintf(buffer, "Marks.Mark_%.3i", i);
                    std::string line = pt.get<std::string>(buffer);
                    tokenizer tokens(line, sep);

                    tokenizer::iterator tok_iter = tokens.begin();
                    Point3f marker;
                    marker.x = ::atof(std::string(*tok_iter++).c_str());
                    marker.y = ::atof(std::string(*tok_iter++).c_str());
                    marker.z = ::atof(std::string(*tok_iter).c_str());

                    //check whether the marker is an id marker
                    sprintf(buffer, "Marks.Mark_%.3i_ID", i);
                    boost::optional <int> id = pt.get_optional<int>(buffer);

                    if (id.is_initialized()) {
                        // add entry to id markers
                        rig.idMarkers.index.push_back(id.get());
                        rig.idMarkers.data.push_back(marker);
                        continue;
                    }
                    //add entry to ring markers
                    rig.ringMarkers.data.push_back(marker);
                }
                return true;
            }
            catch(...) {
                return false;
            }
        }

        bool CalibIniIO::Validate(const char *filename) {
            fileValid = false;
            if(!CheckFile(filename))
                return false;
            try
            {
                boost::property_tree::ini_parser::read_ini(filename, pt);
                fileValid = true;
                return true;
            }
            catch(...) {
                return false;
            }
        }

        bool CalibIniIO::Write(const char* filename, const Image& abs, CorrespondenceAbsolute& corr) const{
            boost::property_tree::ptree pt;
            char bufferHeader[32];
            char bufferData[256];

            int counter = 0;
            for(size_t i = 0; i < corr.geometric.from.size(); i++)
            {
                double imgX = corr.geometric.to[i].x;
                double imgY = corr.geometric.to[i].y;
                double wrdX = corr.geometric.from[i].x;
                double wrdY = corr.geometric.from[i].y;
                double wrdZ = corr.geometric.from[i].z;
                double srcW = corr.absoluteValue[i];

                sprintf(bufferHeader, "RawCalibration.P%.3i", counter++);
                sprintf(bufferData, "%f %f %f %f %f %f", wrdX, wrdY, wrdZ, imgX, imgY, srcW);
                pt.put(bufferHeader, bufferData);
            }
            pt.put("RawCalibration.NP", counter);
            boost::property_tree::write_ini(filename, pt);

            return true;
        }

    } // namespace vision
} // namespace cook
