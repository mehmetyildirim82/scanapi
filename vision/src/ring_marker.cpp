//
// Created by yildirim on 08.06.15.
//

#include "ring_marker.h"
#include <opencv2/imgproc/imgproc.hpp>

namespace cook {
    namespace vision {

        RingMarker::RingMarker() {
            //..
        }

        RingMarker::~RingMarker() {
            //..
        }

        bool RingMarker::FindSelf(const ContourArray& contours, MarkerArray2d& markers, Image* mask, Image* outImg) const {
            try {
                //Find the rotated rectangles and ellipses for each contour
                std::vector<cv::RotatedRect> minEllipse( contours.size() );

                for( size_t i = 0; i < contours.size(); i++ )
                {
                    if( contours[i].size() > 5 )
                    {
                        minEllipse[i] = cv::fitEllipse( cv::Mat(contours[i]) );
                    }
                }

                cv::RNG rng(12345);
                //Draw contours + rotated rects + ellipses
                for( size_t i = 0; i< minEllipse.size(); i++ )
                {
                    cv::RotatedRect& rect = minEllipse[i];
                    if(rect.size.width < minSize.width || rect.size.height < minSize.height)continue;
                    if(rect.size.width > maxSize.width || rect.size.height > maxSize.height)continue;

                    if(outImg)
                        cv::circle( *outImg, rect.center, rect.size.width/2 ,cv::Scalar(255,255,0), 2);

                    markers.data.push_back(rect.center);
                }
                return true;
            }
            catch(...) {
                return false;
            }
        }

    } // namespace vision
} // namespace cook
