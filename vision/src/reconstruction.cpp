//
// Created by yildirim on 27.06.15.
//

#include "reconstruction.h"
#include <numerical.h>

namespace cook {
	namespace vision {

		bool CalculateCloudFromGrayCodeScanData(CorrespondenceAbsolute abs_corr, ScanDataGrayCode::Ptr scan_data, const Image& abs_phase, std::vector<cv::Point3f>& points, std::vector<cv::Scalar>& color, ScanBoundary limits)
		{
			try
			{
				//Calibrate single cam
				CalibrationSingle calib;
				CameraModel cam;
				ProjectorModel proj;
				calib.Calibrate(abs_corr, cam, proj);

				Correspondence2d stereoView;
				Generate2dGrid(Rect(5,5,1275,1020), stereoView.from);

				cv::Vec3b col;
				ImageSequence texture_seq = scan_data->GetDataSection(ScanDataGrayCode::DataSection::TEXTURE);
				Image texture = texture_seq[0];

                for(size_t i = 0; i < stereoView.from.size(); i++)
				{
					Point2f p = stereoView.from[i];
					short srcW = BilinearInterpolation<short>(p, abs_phase);
					if (!srcW)continue;

					Point3f pt3f;
					Estimate3DPoint( p.x, p.y, srcW, cam, proj, pt3f);

					col = texture.at<cv::Vec3b>(p.y, p.x);

					if(pt3f.z < limits.minZ || pt3f.z > limits.maxZ)
						continue;

					if(pt3f.x < limits.minX || pt3f.x > limits.maxX)
						continue;

					if(pt3f.y < limits.minY || pt3f.y > limits.maxY)
						continue;

					points.push_back(pt3f);
					color.push_back(cv::Scalar(col.val[0], col.val[1], col[2]));
				}
				return true;
			}
			catch(...)
			{
				return false;
			}
		}


		bool Estimate3DPoint( const double& U,
			const double& V,
			const double&W,
			CameraModel& camera,
			ProjectorModel& projector,
			Point3f& pt3f) {
				if( !W )
					return false;

				double A[3][3];
				double b[3];
				double x[3];

				ProjectionMatrix& cam = camera.GetProjectionMat();
				Mat& proj = projector.GetModel();

				A[0][0] = cam.At(0,0)-U*cam.At(2,0);
				A[0][1] = cam.At(0,1)-U*cam.At(2,1);
				A[0][2] = cam.At(0,2)-U*cam.At(2,2);

				A[1][0] = cam.At(1,0)-V*cam.At(2,0);
				A[1][1] = cam.At(1,1)-V*cam.At(2,1);
				A[1][2] = cam.At(1,2)-V*cam.At(2,2);

				A[2][0] = proj.At(0,0)-W*proj.At(0,4);
				A[2][1] = proj.At(0,1)-W*proj.At(0,5);
				A[2][2] = proj.At(0,2)-W*proj.At(0,6);

				b[0] = U*cam.At(2,3)-cam.At(0,3);
				b[1] = V*cam.At(2,3)-cam.At(1,3);
				b[2] = W*proj.At(0,7)-proj.At(0,3);

				// Solving linear equation
				if( !Solve3x3LinearEq(A, b, x, 1.0E-12) )
					return false;

				// Save 3d coordinates
				pt3f.x = x[0];
				pt3f.y = x[1];
				pt3f.z = x[2];

				return true;
		}


	} // namespace vision
} // namespace cook
