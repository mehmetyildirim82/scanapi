//
// Created by yildirim on 05.06.15.
//

#include "conversion.h"

namespace cook {
    namespace vision {

        void NormaliseHomogenous(Mat& mat_homo) {
            double w = mat_homo.At(mat_homo.Get().rows-1, mat_homo.Get().cols-1);
            mat_homo.Get() /= w;
        }

    } // namespace vision
} //namespace cook