//
// Created by yildirim on 27.06.15.
//

#include "projector_model.h"

namespace cook {
    namespace vision {

        ProjectorModel::ProjectorModel() :
                model(1,8)
        {
            //
        }

        Mat& ProjectorModel::GetModel() {
            return model;
        }

    } //namespace vision
} // namespace cook