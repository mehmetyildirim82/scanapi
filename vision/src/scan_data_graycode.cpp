//
// Created by yildirim on 27.06.15.
//
#include <opencv2/imgproc/imgproc.hpp>
#include <scan_data_graycode.h>

namespace cook {
    namespace vision{

        ScanDataGrayCode::ScanDataGrayCode() {
            sectionSize.push_back(1);
            sectionSize.push_back(16);
            sectionSize.push_back(4);
            sequences.resize(3);
        }

        bool ScanDataGrayCode::AddImage(Image scan_image) {
            // If container is full already, don't add image to the container
            if(activeSection >= sequences.size())
                return false;

            PreProcess(scan_image);

            //Add image to container
            sequences[activeSection].push_back(scan_image);

            // Change active section if necessary
            if(sequences[activeSection].size() == sectionSize[activeSection])
                activeSection++;
        }

        ImageSequence& ScanDataGrayCode::GetDataSection(DataSection section) {
            return sequences[section];
        }

        void ScanDataGrayCode::PreProcess(Image& scan_image) {
			if(activeSection != TEXTURE && scan_image.type() == 16)
                cv::cvtColor(scan_image, scan_image, CV_BGR2GRAY);
        }	

    } // namespace vision
} // namespace cook
