//
// Created by yildirim on 21.06.15.
//

#include "calibration_stereo.h"

#include <opencv2/calib3d/calib3d.hpp>
#include <exceptions.h>

namespace cook {
    namespace vision{

        double CalibrationStereo::Calibrate(const StereoCorrespondence& correspondence,
                         StereoModel& model)
        {
            try {
                CameraModel& cam01 = model.GetCamera(StereoModel::Camera1);
                CameraModel& cam02 = model.GetCamera(StereoModel::Camera2);

                std::vector<Point3dArray> world(1);
                world[0] = correspondence.world;
                std::vector<Point2dArray> pts01(1);
                std::vector<Point2dArray> pts02(1);
                pts01[0] = correspondence.image01;
                pts02[0] = correspondence.image02;

                double RMS = cv::stereoCalibrate(
                        world,
                        pts01, // Points from first image
                        pts02, // Points from second image
                        cam01.GetIntrinsics().Get(), // K from cam1
                        cam01.GetDistortionCoeff(), // D from cam1
                        cam02.GetIntrinsics().Get(), // K from cam1
                        cam02.GetDistortionCoeff(), // D from cam1
                        cam01.GetImageSize(),
                        model.GetRotation().Get(),
                        model.GetTranslation().Get(),
                        model.GetEssentialMatrix().Get(),
                        model.GetFundamentalMatrix().Get(),
                        cv::TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 50, 1e-5),
                        CV_CALIB_USE_INTRINSIC_GUESS
                );
                return RMS;
            }
            catch (...) {
                throw exceptionOpenCV();
            }
        }

    } // namespace vision
} // namespace cook