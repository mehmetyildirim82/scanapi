//
// Created by yildirim on 01.06.15.
//
#include "projection_matrix.h"
#include <opencv2/calib3d/calib3d.hpp>

#include <exceptions.h>
#include <conversion.h>
#include <iostream>

namespace cook{
    namespace vision{

        ProjectionMatrix::ProjectionMatrix() : Mat( 3, 4 ) {
            //
        }

        ProjectionMatrix::ProjectionMatrix(double data[]) : Mat(3,4, data) {
            //
        }

        ProjectionMatrix::ProjectionMatrix(Mat& mat) : Mat(3,4) {
            if(mat.GetReadOnly().size != this->mat.size)
                throw exceptionMatrixSize();
            this->mat = mat.Get().clone();
        }

        ProjectionMatrix::ProjectionMatrix(const ProjectionMatrix& projMat) {
            *this = projMat;
        }

        ProjectionMatrix& ProjectionMatrix::operator=(const ProjectionMatrix& rhs) {
            mat = rhs.GetReadOnly().clone();
            return *this;
        }

        ProjectionMatrix::~ProjectionMatrix(){
            //...
        }

        Point2f ProjectionMatrix::Project(const Point3f &p3d) const {
            Mat result = HomogenizeAndMultiply(p3d, mat);
            NormaliseHomogenous(result);
            return Point2f(result.At(0,0), result.At(1,0));
        }

        float ProjectionMatrix::TestProjection(const Point3f& p3f, const Point2f& p2f)const {
            Point2f projected = Project(p3f);
            return sqrt((projected.x - p2f.x)*(projected.x - p2f.x) + (projected.y - p2f.y)*(projected.y - p2f.y)); //RMS
        }

        bool ProjectionMatrix::Compose(const IntrinsicMat& K, const Mat& rotation, const Mat& translation) {
            //P = [K*R -(K*R)*C];
            cv::Mat rotmat;
            if(rotation.GetReadOnly().cols == 1)
            {
                cv::Rodrigues(rotation.GetReadOnly(), rotmat);

                cv::Mat exterior = cv::Mat::zeros(4,4,CV_64F);
                cv::Mat middle = cv::Mat::eye(3,4,CV_64F);
                cv::Mat PP1 = cv::Mat::zeros(3,4,CV_64F);

                exterior.at<double>(0,0) = rotmat.at<double>(0,0);
                exterior.at<double>(0,1) = rotmat.at<double>(0,1);
                exterior.at<double>(0,2) = rotmat.at<double>(0,2);
                exterior.at<double>(1,0) = rotmat.at<double>(1,0);
                exterior.at<double>(1,1) = rotmat.at<double>(1,1);
                exterior.at<double>(1,2) = rotmat.at<double>(1,2);
                exterior.at<double>(2,0) = rotmat.at<double>(2,0);
                exterior.at<double>(2,1) = rotmat.at<double>(2,1);
                exterior.at<double>(2,2) = rotmat.at<double>(2,2);
                exterior.at<double>(0,3) = translation.GetReadOnly().at<double>(0);
                exterior.at<double>(1,3) = translation.GetReadOnly().at<double>(1);
                exterior.at<double>(2,3) = translation.GetReadOnly().at<double>(2);
                exterior.at<double>(3,0) = 0;
                exterior.at<double>(3,1) = 0;
                exterior.at<double>(3,2) = 0;
                exterior.at<double>(3,3) = 1;

                PP1 = K.GetReadOnly() * middle * exterior;
                PP1 = PP1 / PP1.at<double>(2,3);
                this->mat = PP1;
            }
            else
            {
                rotmat = rotation.GetReadOnly();

                cv::Mat KR = K.GetReadOnly()*rotmat;
                cv::Mat minKRC = -KR*translation.GetReadOnly();

                this->At(0,0) = KR.at<double>(0,0);
                this->At(0,1) = KR.at<double>(0,1);
                this->At(0,2) = KR.at<double>(0,2);
                this->At(1,0) = KR.at<double>(1,0);
                this->At(1,1) = KR.at<double>(1,1);
                this->At(1,2) = KR.at<double>(1,2);
                this->At(2,0) = KR.at<double>(2,0);
                this->At(2,1) = KR.at<double>(2,1);
                this->At(2,2) = KR.at<double>(2,2);

                this->At(0,3) = minKRC.at<double>(0);
                this->At(1,3) = minKRC.at<double>(1);
                this->At(2,3) = minKRC.at<double>(2);
            }

            return true;
        }

        bool ProjectionMatrix::Decompose(IntrinsicMat& K,
                                         Mat& rotation,
                                         Mat& translation)const {
            try {
                cv::decomposeProjectionMatrix(mat, K.Get(), rotation.Get(), translation.Get());
                NormaliseHomogenous(translation);
                translation.DeleteLasRow();

                NormaliseHomogenous(K);
                return true;
            }
            catch(...) {
                return false;
            }
        }

    } // namespace vision
} // namespace cook
