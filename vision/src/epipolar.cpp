//
// Created by yildirim on 05.07.15.
//

#include "epipolar.h"
#include <opencv2/calib3d/calib3d.hpp>
#include <iostream>

namespace cook {
    namespace vision {

        bool ComputeEpipolarLines(const Point2dArray& points, const Mat& fundamentalMatrix, EpipolarBundle& bundle) {
            try {
                cv::computeCorrespondEpilines(points, 1, fundamentalMatrix.GetReadOnly(), bundle);
                return true;
            }
            catch (...) {
                return false;
            }
        }

        bool ReduceEpipolarSearchSpace(const Point2f& ptEstimate, float limit, Line2d& l) {
            //project approx image point on the destination epipolar line
            double alpha = AngleBetween(l.direction, ptEstimate-l.center);

            Point2f dest;
            float n = cv::norm(l.direction);
            dest.x = l.direction.x / n;
            dest.y = l.direction.y / n;

            float dist = cv::norm(l.center - ptEstimate);

            Point2f projection = l.center + ( std::cos(alpha) * dist) * dest;
            l.center.x = projection.x - (limit * dest.x);
            l.center.y = projection.y - (limit * dest.y);

            l.direction.x = (projection.x + (limit*dest.x)) - (l.center.x);
            l.direction.y = (projection.y + (limit*dest.y)) - (l.center.y);

            return true;
        }

    } // namespace vision
} // namespace cook