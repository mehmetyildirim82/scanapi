//
// Created by yildirim on 19.06.15.

#include <visualization.h>
#include <sstream>
#include <opencv2/highgui/highgui.hpp>
#include "types_vision.h"

namespace cook {
    namespace vision {

        Image Visualize(const Image& imgIn, IdMarkerArray2d& id_markers, const char* filename) {
            Image result = imgIn.clone();

            for(int i = 0; i < id_markers.index.size(); i++) {
                cv::circle( result, id_markers.data[i], 20, cv::Scalar(255,0,0), 1);
                std::ostringstream oss;
                oss << id_markers.index[i];
                cv::putText( result, oss.str(), id_markers.data[i] - cv::Point2f(15,0) , cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 255, 0), 2);
            }

            if(filename)
                cv::imwrite(filename, result);
            return result;
        }

        Image Visualize(const Image& imgIn, Point2dArray& markers, const char* filename) {
            Image result = imgIn.clone();

            for(int i = 0; i < markers.size(); i++) {
                cv::circle(result, markers[i], 2, cv::Scalar(100,0,0), 1);
            }

            if(filename)
                cv::imwrite(filename, result);
            return result;
        }

        Image Visualize(const Image& imgIn, Line2d l, const char* filename, int thickness) {
            Image result = imgIn.clone();

            cv::line(result, l.center, l.center+l.direction, cv::Scalar(255), thickness);

            if(filename)
                cv::imwrite(filename, result);
            return result;
        }

    }
}

