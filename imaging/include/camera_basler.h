//
// Created by yildirim on 15.08.15.
//

#ifndef ORIENTSCAN3D_CAMERA_BASLER_H
#define ORIENTSCAN3D_CAMERA_BASLER_H

#include <camera.h>
#include <pylon/PylonIncludes.h>

namespace cook {
    namespace imaging {

        //TODO: Documentation!
        class CameraBasler : public Camera {
        public:

            CameraBasler();
			~CameraBasler();

            bool Initialize();

            static void InitializeDriver();

            bool Open(int idx);

            bool Close();

            void Start();

            void Stop();

            bool SnapAsync();

            bool SnapSync();

            bool Wait();

            bool GetFrame(Image &img);

			static void Terminate();

			std::string GetDescription();

            bool isRemoved();

            static std::size_t GetNumberOfDevices();

			double GetShutterTime();

			void SetShutterTime(double shutter_time);

            typedef std::shared_ptr <CameraBasler> Ptr;

        private:
            Pylon::CInstantCamera camera;
			Image image16;
            static bool isInitialized;
            static unsigned int s_index;
            std::string serial;
            std::string name;
            unsigned int index;

        };

    }
}

#endif //ORIENTSCAN3D_CAMERA_BASLER_H
