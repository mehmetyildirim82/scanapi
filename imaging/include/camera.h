//
// Created by yildirim on 08.07.15.
//

#ifndef ORIENTSCAN3D_CAMERA_H
#define ORIENTSCAN3D_CAMERA_H

#include <memory>
#include <string>

#include <basic_types.h>

namespace cook {
    namespace imaging {

        //TODO: Documentation!
        class Camera {
        public:
            Camera(){}
            virtual ~Camera(){};
            virtual bool Initialize()=0;
            virtual bool Open(int idx)=0;
            virtual bool Close()=0;
            virtual void Start()=0;
            virtual void Stop()=0;
            virtual bool SnapAsync()=0;
            virtual bool SnapSync()=0;
            virtual bool Wait()=0;
            virtual bool GetFrame(Image& img)=0;
			virtual std::string GetDescription()=0;
            virtual bool isRemoved()=0;

            virtual double GetShutterTime()=0;
			virtual void SetShutterTime(double shutter_time)=0;

            typedef std::shared_ptr<Camera> Ptr;
            bool started;
        protected:
            int manager_idx;
        };
    }
}

#endif //ORIENTSCAN3D_CAMERA_H
