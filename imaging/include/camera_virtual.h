//
// Created by yildirim on 08.07.15.
//

#ifndef ORIENTSCAN3D_CAMERA_VIRTUAL_H
#define ORIENTSCAN3D_CAMERA_VIRTUAL_H

#include <camera.h>

namespace cook {
    namespace imaging {

        class CameraVirtual : public Camera {
        public:
            CameraVirtual();
            ~CameraVirtual();
            bool Initialize();
            bool Open(int idx);
            bool Close();
            void Start();
            void Stop();
            bool SnapAsync();
            bool SnapSync();
            bool Wait();
            bool GetFrame(Image& img);
			std::string GetDescription();
            bool isRemoved();
			virtual double GetShutterTime(){return 0.0;}
			virtual void SetShutterTime(double shutter_time){};

            void AddFrameToBuffer(Image img);

			typedef std::shared_ptr<CameraVirtual> Ptr;

            private:
            std::vector<Image> buffer;
            int bufferCursor;
			int camIdx;
			static int s_camIdx;
        };

    }
}

#endif //ORIENTSCAN3D_CAMERA_VIRTUAL_H
