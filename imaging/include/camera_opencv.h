//
// Created by yildirim on 08.07.15.
//

#ifndef ORIENTSCAN3D_CAMERA_OPENCV_H
#define ORIENTSCAN3D_CAMERA_OPENCV_H

#include <opencv2/opencv.hpp>

#include <camera.h>
#include <basic_types.h>

namespace cook {
    namespace imaging {

        //TODO: Documentation!
        class CameraOpenCV : public Camera {
        public:

            bool Initialize();
            bool Open(int idx);
            bool Close();
            void Start();
            void Stop();
            bool SnapAsync();
            bool SnapSync();
            bool Wait();
            bool GetFrame(Image& img);
            bool isRemoved();
			std::string GetDescription();
			virtual double GetShutterTime(){return 0.0;}
			virtual void SetShutterTime(double shutter_time){};

            typedef std::shared_ptr<CameraOpenCV> Ptr;

        private:
            cv::VideoCapture capture;
        };
    }
}

#endif //ORIENTSCAN3D_CAMERA_OPENCV_H
