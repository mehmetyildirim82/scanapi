#include <gtest/gtest.h>

#include <camera_opencv.h>
#include <opencv2/highgui/highgui.hpp>

using namespace cook::imaging;
using namespace cook;

TEST(CameraOpencv, Initialization)
{
    CameraOpenCV::Ptr camOcv(new CameraOpenCV);
    Camera::Ptr cam(camOcv);
    cam->GetShutterTime() = 100.0;

#ifdef RUN_CAMERA_TESTS
    cam->Open(0);
    Image img;
    cam->SnapSync();
    if(cam->Wait()) {
        cam->GetFrame(img);
    }
    cv::imwrite("camera_virtual_test.bmp", img);
#endif
    EXPECT_EQ(cam->GetShutterTime(), 100.0);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}