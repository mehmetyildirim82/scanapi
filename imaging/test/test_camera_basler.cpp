//
// Created by yildirim on 16.08.15.
//

#include <gtest/gtest.h>

#include <camera_basler.h>
#include <opencv2/highgui/highgui.hpp>
//#include "../../../../../../../usr/include/x86_64-linux-gnu/sys/types.h"

using namespace cook::imaging;
using namespace cook;

#ifdef RUN_CAMERA_TESTS
TEST(CameraBasler, Initialization)
{
    CameraBasler camL, camR;
    EXPECT_TRUE(camL.Initialize());

    EXPECT_TRUE(camL.Open(0));
    EXPECT_TRUE(camR.Open(1));

    camL.SnapAsync();
    camR.SnapAsync();

    EXPECT_TRUE(camL.Wait());
    EXPECT_TRUE(camR.Wait());

    Image img = cv::Mat(1024, 1280, CV_8UC1);
    camL.GetFrame(img);
    cv::imwrite("imgL.bmp", img);

    camR.GetFrame(img);
    cv::imwrite("imgR.bmp", img);
}
#endif

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}