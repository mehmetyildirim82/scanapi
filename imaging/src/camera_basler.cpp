//
// Created by yildirim on 15.08.15.
//

#include "camera_basler.h"
#include <opencv2\imgproc\imgproc.hpp>

using namespace std;

namespace cook {
    namespace imaging {

        CameraBasler::CameraBasler()
        {
            manager_idx = -1;
            started = false;
        }

		CameraBasler::~CameraBasler()
        {
            if(s_index != 0) s_index--;
		}

		bool CameraBasler::isInitialized = false;
        unsigned int CameraBasler::s_index = 0;

        bool CameraBasler::Initialize() {
           //Initialize the camera driver
            CameraBasler::InitializeDriver();
            return isInitialized;
        }

        void CameraBasler::InitializeDriver() {
            if(!isInitialized) {
                Pylon::PylonInitialize();
            }
            isInitialized = true;
        }

        void CameraBasler::Terminate() {
            if(isInitialized) {
                Pylon::PylonTerminate();
                isInitialized = false;
            }
        }

        bool CameraBasler::Open(int idx) {
            if(!isInitialized) {
                cout << "camera driver is not initialized!!" << endl;
                return false;
            }

            if(s_index == 0 || manager_idx == -1) {
                index = s_index++;
                manager_idx = idx;
            }

            image16 = cv::Mat::zeros(1024, 1280, CV_8UC2);

            Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();
            Pylon::DeviceInfoList_t listDevices;
            tlFactory.EnumerateDevices(listDevices);

            if(index >= listDevices.size()) {
				cout << "device indexing problem: device size = " << listDevices.size() << endl;
                return false;
            }
				
            try{
                if(!camera.IsPylonDeviceAttached()) camera.Attach(tlFactory.CreateDevice(listDevices[index]));

                camera.RegisterConfiguration(new Pylon::CSoftwareTriggerConfiguration, Pylon::RegistrationMode_ReplaceAll, Pylon::Cleanup_Delete);

                camera.MaxNumBuffer = 15;

                camera.Open();

                GenApi::INodeMap& nodemap = camera.GetNodeMap();
                // Access the PixelFormat enumeration type node.
                GenApi::CEnumerationPtr pixelFormat(nodemap.GetNode("PixelFormat"));

                // Remember the current pixel format.
                Pylon::String_t oldPixelFormat = pixelFormat->ToString();
                //cout << "PixelFormat  : " << oldPixelFormat << endl;

                //camera.GetDeviceInfo().GetVendorName();
                name = camera.GetDeviceInfo().GetModelName();
                serial = camera.GetDeviceInfo().GetSerialNumber();
            }
            catch (...){
                return false;
            }

            cout << "Opened device " << name << " serial: " << serial << " index: " << index << endl;
            return true;
        }

        bool CameraBasler::Close() {
            cout << "Closing device " << name << " serial: " << serial << " index: " << index << endl;
            camera.Close();
            return true;
        }

        void CameraBasler::Start() {
            if(started) return;
            camera.StartGrabbing(Pylon::GrabStrategy_OneByOne);
            cout << "started grabbing cam " << name << " (" << serial << ") " << endl;
            started = true;
        }

        void CameraBasler::Stop() {
            if(!started) return;
            camera.StopGrabbing();
            cout << "stopped grabbing cam " << name << " (" << serial << ") " << endl;
            started = false;
        }

        bool CameraBasler::SnapAsync() {
            if(!started) return false;
			if ( camera.WaitForFrameTriggerReady( 1000 ))
			{
				camera.ExecuteSoftwareTrigger();
			}
            return true;
        }

        bool CameraBasler::SnapSync() {
            return true;
        }

        bool CameraBasler::Wait() {
            if(!started) return false;
            return camera.GetGrabResultWaitObject().Wait( 1000 );
        }

		std::string CameraBasler::GetDescription()
		{
            std::ostringstream oss;
            oss << "Basler Camera - " << name << " (" << serial << ") ";
			return oss.str().c_str();
		}

        bool CameraBasler::isRemoved()
        {
            return camera.IsCameraDeviceRemoved();
        }

        std::size_t CameraBasler::GetNumberOfDevices() {
            Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();
            Pylon::DeviceInfoList_t listDevices;
            tlFactory.EnumerateDevices(listDevices);

            return listDevices.size();
        }

		double CameraBasler::GetShutterTime()
		{
			GenApi::INodeMap& nodemap = camera.GetNodeMap();
			GenApi::CFloatPtr exposure_time( nodemap.GetNode( "ExposureTimeAbs"));

            double result = exposure_time->GetValue() / 1000.0;
			return result;
		}

		void CameraBasler::SetShutterTime(double shutter_time)
		{
            //cout << "shutter time setting to: " << shutter_time << endl;
			GenApi::INodeMap& nodemap = camera.GetNodeMap();
            GenApi::CFloatPtr exposure_time(nodemap.GetNode("ExposureTimeAbs"));
			exposure_time->SetValue((float)shutter_time);
            //std::cout << "exposuremax: " << exposure_time->GetMax() << "exposuremin: " << exposure_time->GetMin() <<  std::endl;
		}

        bool CameraBasler::GetFrame(Image &img) {
            if(!started) return false;
            Pylon::CGrabResultPtr ptrGrabResult;
            camera.RetrieveResult( 0, ptrGrabResult, Pylon::TimeoutHandling_Return);

			/*
            cout << "Height of grabbed image: " << ptrGrabResult->GetHeight() << endl;
            cout << "Image size: " << ptrGrabResult->GetImageSize() << endl;
            cout << "Image number: " << ptrGrabResult->GetImageNumber() << endl;
            cout << "Pixel: " << ptrGrabResult->GetPixelType() << endl;

            size_t stride;
            ptrGrabResult->GetStride(stride);
            cout << "Stride: " << stride << endl;
			*/

            unsigned char* data = static_cast<unsigned char*>(ptrGrabResult->GetBuffer());
            memcpy(image16.data, data, ptrGrabResult->GetImageSize());

			cv::cvtColor(image16, img, CV_YUV2BGR_Y422);

            return true;
        }
    }
}
