//
// Created by yildirim on 08.07.15.
//

#include "camera_virtual.h"

#include <sstream>
#include <iostream>

namespace cook {
    namespace imaging {

        int CameraVirtual::s_camIdx = 0;

        CameraVirtual::CameraVirtual()
        {
            manager_idx = -1;
            started = false;
        }

        CameraVirtual::~CameraVirtual()
        {
            if(s_camIdx != 0) s_camIdx--;
        }

        bool CameraVirtual::Initialize()
		{
			bufferCursor = 0;
			return true;
		}

        bool CameraVirtual::Open(int idx)
		{
            if(s_camIdx == 0 || manager_idx == -1) {
                camIdx = s_camIdx++;
                manager_idx = idx;
            }
			return true;
		}

		void CameraVirtual::AddFrameToBuffer(Image img)
		{
			buffer.push_back(img);
		}
         
		bool CameraVirtual::Close()
		{
			return true;
		}

        void CameraVirtual::Start()
        {
            started = true;
        }

        void CameraVirtual::Stop()
        {
            started = false;
        }

		bool CameraVirtual::SnapAsync()
		{
			return true;
		}

        bool CameraVirtual::SnapSync()
		{
			return true;
		}

        bool CameraVirtual::Wait()
		{
			return true;
		}
         
		bool CameraVirtual::GetFrame(Image& img)
		{
			img = buffer[bufferCursor%buffer.size()].clone();
			bufferCursor++;
			return true;
		}

		std::string CameraVirtual::GetDescription()
		{
			std::ostringstream oss;
			oss << "Virtual Camera - " << camIdx;
			return oss.str();
		}

        bool CameraVirtual::isRemoved()
        {
            return false;
        }
    }
}
