//
// Created by yildirim on 08.07.15.
//

#include "camera_opencv.h"

namespace cook {
    namespace imaging {

        bool CameraOpenCV::Initialize() {
            return true;
        }

        bool CameraOpenCV::Open(int idx) {
            return capture.open(idx);
        }

        bool CameraOpenCV::Close() {
            try {
                capture.release();
                return true;
            }
            catch (...) {
                return false;
            }
        }

        void CameraOpenCV::Start()
        {
            started = true;
        }

        void CameraOpenCV::Stop()
        {
            started = false;
        }

        bool CameraOpenCV::GetFrame(Image& img) {
            return capture.retrieve(img);
        }

        bool CameraOpenCV::Wait() {
            return true;
        }

        bool CameraOpenCV::SnapAsync() {
            return true;
        }

        bool CameraOpenCV::SnapSync() {
            return capture.grab();
        }

		std::string CameraOpenCV::GetDescription()
		{
			return std::string("Basler");
		}

        bool CameraOpenCV::isRemoved()
        {
            return false; //dummy
        }

    }
}
